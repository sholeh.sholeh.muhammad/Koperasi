/*
Navicat MySQL Data Transfer

Source Server         : mysql
Source Server Version : 50621
Source Host           : localhost:3306
Source Database       : db_latihan

Target Server Type    : MYSQL
Target Server Version : 50621
File Encoding         : 65001

Date: 2017-04-28 08:58:17
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `menu`
-- ----------------------------
DROP TABLE IF EXISTS `menu`;
CREATE TABLE `menu` (
  `menu_id` int(11) NOT NULL AUTO_INCREMENT,
  `men_menu_id` int(11) DEFAULT NULL,
  `menu_name` varchar(50) NOT NULL,
  `menu_link` varchar(100) NOT NULL,
  `menu_status` tinyint(1) NOT NULL,
  `menu_ismaster` tinyint(1) NOT NULL,
  `menu_order` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `is_deleted` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`menu_id`),
  KEY `fk_parent_id` (`men_menu_id`)
) ENGINE=MyISAM AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu
-- ----------------------------
INSERT INTO `menu` VALUES ('15', null, 'Pengaturan', '#', '1', '1', '15', null, null, null, null, null);
INSERT INTO `menu` VALUES ('16', '15', 'User', 'admin/user', '1', '1', '16', null, null, null, null, null);
INSERT INTO `menu` VALUES ('17', '15', 'Menu', 'admin/menu', '1', '1', '17', null, null, null, null, null);
INSERT INTO `menu` VALUES ('18', '15', 'Hak Akses', 'admin/role', '1', '1', '18', null, null, null, null, null);
INSERT INTO `menu` VALUES ('19', '15', 'Menu Role', 'menu_role', '0', '1', '19', null, null, null, null, null);
INSERT INTO `menu` VALUES ('38', null, 'Dashboard', 'admin/dashboard', '1', '0', '1', '2016-05-05 12:27:56', 'super', null, null, null);
INSERT INTO `menu` VALUES ('48', null, 'Kategori Produk', 'admin/product_category', '1', '1', '9', null, null, null, null, null);
INSERT INTO `menu` VALUES ('52', null, 'Produk', 'admin/product', '1', '0', '7', null, null, null, null, null);

-- ----------------------------
-- Table structure for `menu_role`
-- ----------------------------
DROP TABLE IF EXISTS `menu_role`;
CREATE TABLE `menu_role` (
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `CREATED_ON` datetime DEFAULT NULL,
  `UPDATED_BY` int(11) DEFAULT NULL,
  `CREATED_BY` int(11) DEFAULT NULL,
  `UPDATED_ON` datetime DEFAULT NULL,
  `IS_DELETED` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`role_id`,`menu_id`),
  KEY `fk_menu_role2` (`menu_id`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of menu_role
-- ----------------------------
INSERT INTO `menu_role` VALUES ('1', '45', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '44', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '43', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '41', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '40', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '19', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '18', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '17', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '16', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '42', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '39', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '38', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '15', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '46', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '47', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '48', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '49', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '51', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '52', null, null, null, null, null);
INSERT INTO `menu_role` VALUES ('1', '53', null, null, null, null, null);

-- ----------------------------
-- Table structure for `product`
-- ----------------------------
DROP TABLE IF EXISTS `product`;
CREATE TABLE `product` (
  `product_id` int(11) NOT NULL AUTO_INCREMENT,
  `cat_id` int(11) NOT NULL,
  `product_name` varchar(150) NOT NULL,
  `product_description` text NOT NULL,
  `product_price` float NOT NULL,
  `product_stock` int(11) DEFAULT NULL,
  `product_discount` float DEFAULT NULL,
  `product_sold` int(11) NOT NULL,
  `product_weight` float NOT NULL,
  `product_status` tinyint(1) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `product_material` text,
  PRIMARY KEY (`product_id`),
  KEY `fk_r_1` (`cat_id`),
  CONSTRAINT `product_ibfk_1` FOREIGN KEY (`cat_id`) REFERENCES `product_category` (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=62 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product
-- ----------------------------
INSERT INTO `product` VALUES ('55', '22', 'sendal', 'aaaaa', '0', null, null, '0', '0', '1', 'admin', '2017-03-23 11:22:24', null, null, 'aa');
INSERT INTO `product` VALUES ('57', '22', 'sendal', 'aaaaa', '0', null, null, '0', '1', '1', 'admin', '2017-03-23 11:24:02', null, null, 'aaaa');
INSERT INTO `product` VALUES ('58', '22', 'Product Test', 'deskripsi', '0', null, null, '0', '1000', '1', 'admin', '2017-03-23 12:25:24', null, null, 'Spons');
INSERT INTO `product` VALUES ('60', '23', 'tete', 'gfhfghgfh', '45545', '344', '12', '0', '122', '1', 'super', '2017-04-24 22:53:29', null, null, null);
INSERT INTO `product` VALUES ('61', '22', 'fgdf', 'dfgdfg', '32234', '234', '34', '0', '324', '1', 'super', '2017-04-24 23:02:13', null, null, null);

-- ----------------------------
-- Table structure for `product_category`
-- ----------------------------
DROP TABLE IF EXISTS `product_category`;
CREATE TABLE `product_category` (
  `cat_id` int(11) NOT NULL AUTO_INCREMENT,
  `parent_cat_id` int(11) DEFAULT NULL,
  `cat_name` varchar(100) NOT NULL,
  `cat_image` varchar(255) DEFAULT NULL,
  `cat_permalink` varchar(100) DEFAULT NULL,
  `cat_status` tinyint(1) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  PRIMARY KEY (`cat_id`),
  KEY `fk_parent_category` (`parent_cat_id`),
  CONSTRAINT `product_category_ibfk_1` FOREIGN KEY (`parent_cat_id`) REFERENCES `product_category` (`cat_id`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_category
-- ----------------------------
INSERT INTO `product_category` VALUES ('22', null, 'jaket', 'upload/cat_image/a28d3a5a365a0c1f73537baf257dddb6.png', 'jaket', '1', 'admin', '2017-03-23 10:56:11', 'admin', '2017-03-24 18:19:17');
INSERT INTO `product_category` VALUES ('23', null, 'sepatu', 'upload/cat_image/eaff1f353974a620f4b6700eee2f33b9.png', 'sepatu', '1', 'admin', '2017-03-31 15:06:56', null, null);
INSERT INTO `product_category` VALUES ('25', null, 't-shirt', 'upload/cat_image/2324a70e26af5edfdcc3a48e4e9837b0.png', 't-shirt', '1', 'admin', '2017-04-05 13:56:58', null, null);
INSERT INTO `product_category` VALUES ('26', null, 'UNCATEGORIZED', null, null, '0', null, null, null, null);
INSERT INTO `product_category` VALUES ('27', '22', 'Bomber ', null, 'jaket/bom', '1', 'super', '2017-04-24 08:49:00', 'super', '2017-04-24 09:02:21');
INSERT INTO `product_category` VALUES ('29', '23', 'Sneaker', null, 'sneaker1', '1', 'super', '2017-04-24 09:04:57', 'super', '2017-04-24 09:05:58');

-- ----------------------------
-- Table structure for `product_photo`
-- ----------------------------
DROP TABLE IF EXISTS `product_photo`;
CREATE TABLE `product_photo` (
  `product_id` int(11) DEFAULT NULL,
  `prodphoto_path` varchar(255) NOT NULL,
  `prodphoto_description` tinyint(1) DEFAULT NULL,
  `prodphoto_isprimary` tinyint(1) NOT NULL,
  `created_by` varchar(100) DEFAULT NULL,
  `created_on` datetime DEFAULT NULL,
  `updated_by` varchar(100) DEFAULT NULL,
  `updated_on` datetime DEFAULT NULL,
  `prodphoto_token` varchar(255) NOT NULL,
  PRIMARY KEY (`prodphoto_path`),
  KEY `product_id` (`product_id`),
  CONSTRAINT `product_photo_ibfk_1` FOREIGN KEY (`product_id`) REFERENCES `product` (`product_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of product_photo
-- ----------------------------
INSERT INTO `product_photo` VALUES ('40', 'upload/prod_photo/40/5968f318ed450126a74d908ae8c2f42e.png', null, '1', null, null, null, null, '');
INSERT INTO `product_photo` VALUES ('61', 'upload/prod_photo/77bc087ebe9f3226fe128e96c792b80e.png', null, '0', null, null, null, null, '');

-- ----------------------------
-- Table structure for `role`
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role` (
  `role_id` int(11) NOT NULL AUTO_INCREMENT,
  `role_name` varchar(50) NOT NULL,
  `role_status` tinyint(1) NOT NULL,
  `role_canlogin` tinyint(1) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `is_deleted` smallint(6) DEFAULT NULL,
  PRIMARY KEY (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES ('1', 'Super Admin', '1', '1', '0000-00-00 00:00:00', null, '2016-05-21 19:13:08', 'admin', null);
INSERT INTO `role` VALUES ('7', 'Administrator', '1', '1', '0000-00-00 00:00:00', 'super', '2016-05-21 19:13:35', 'admin', null);

-- ----------------------------
-- Table structure for `user`
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user` (
  `id_user` int(11) NOT NULL AUTO_INCREMENT,
  `role_id` int(11) NOT NULL,
  `username` varchar(10) DEFAULT NULL,
  `password` varchar(255) DEFAULT NULL,
  `full_name` varchar(50) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `created_by` varchar(10) DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `updated_by` varchar(10) DEFAULT NULL,
  `is_deleted` smallint(6) DEFAULT NULL,
  `id_clinic` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_user`),
  KEY `fk_user_role` (`role_id`)
) ENGINE=MyISAM AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES ('1', '1', 'admin', '21232f297a57a5a743894a0e4a801fc3', 'Administrator Super', '0000-00-00 00:00:00', 'super', '2017-04-28 03:52:56', 'super', null, null);
