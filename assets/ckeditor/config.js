/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here. For example:
    config.language = 'en-au';
    var filemanager = base_url + "assets/js/ckeditor/filemanager/";
    config.filebrowserUploadUrl = base_url + '/admin/dashboard/upload_file';
    config.filebrowserBrowseUrl = filemanager + 'browser/default/browser.html?Connector='+filemanager+'connectors/php/connector.php';
    config.filebrowserImageBrowseUrl = filemanager + 'browser/default/browser.html?Type=Image&Connector='+filemanager+'connectors/php/connector.php';
    config.extraPlugins = 'youtube';
    // config.uiColor = '#AADC6E';
    config.toolbar = [
    {
        name: 'clipboard', 
        groups: [ 'clipboard' ], 
        items: [ 'PasteText', 'PasteFromWord' ]
    },
    {
        name: 'basicstyles', 
        groups: [ 'basicstyles', 'cleanup' ], 
        items: [ 'Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat' ]
    },
    {
        name: 'paragraph', 
        groups: [ 'list', 'indent', 'blocks', 'align', 'bidi' ], 
        items: [ 'NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock' ]
    },
    {
        name: 'links', 
        items: [ 'Link', 'Unlink' ]
    },
    {
        name: 'styles', 
        items: [ 'Format', 'Font', 'FontSize' ]
    },
    {
        name: 'colors', 
        items: [ 'TextColor', 'BGColor' ]
    },
    {
        name: 'insert',      
        items : [ 'Image','Youtube' ]
    }

    ];

    // Toolbar groups configuration.
    config.toolbarGroups = [
    {
        name: 'clipboard', 
        groups: [ 'clipboard']
    },
    {
        name: 'basicstyles', 
        groups: [ 'basicstyles', 'cleanup' ]
    },
    {
        name: 'paragraph', 
        groups: [ 'list', 'indent', 'blocks', 'align' ]
    },
    {
        name: 'links'
    },
    {
        name: 'styles'
    },
    {
        name: 'colors'
    },
    {
        name: 'insert'
    }
    ];
};
CKEDITOR.on('dialogDefinition', function(ev) {
    var dialogName = ev.data.name,
        dialogDefinition = ev.data.definition;
    if (dialogName === 'image') {
        dialogDefinition.removeContents('Upload');
        dialogDefinition.removeContents('Link');
        dialogDefinition.removeContents('advanced');
    }
});