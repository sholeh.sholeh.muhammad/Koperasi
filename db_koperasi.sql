/*
SQLyog Ultimate v12.09 (64 bit)
MySQL - 10.1.21-MariaDB : Database - db_koperasi
*********************************************************************
*/

/*!40101 SET NAMES utf8 */;

/*!40101 SET SQL_MODE=''*/;

/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
CREATE DATABASE /*!32312 IF NOT EXISTS*/`db_koperasi` /*!40100 DEFAULT CHARACTER SET latin1 */;

USE `db_koperasi`;

/*Table structure for table `anggota` */

DROP TABLE IF EXISTS `anggota`;

CREATE TABLE `anggota` (
  `id_anggota` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_telp` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `j_kelamin` char(10) NOT NULL,
  `status` varchar(255) NOT NULL DEFAULT 'Aktif',
  `keterangan` text NOT NULL,
  `tgl_pendaftaran` date NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id_anggota`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `anggota` */

/*Table structure for table `angsuran` */

DROP TABLE IF EXISTS `angsuran`;

CREATE TABLE `angsuran` (
  `id_angsuran` int(11) NOT NULL AUTO_INCREMENT,
  `id_pinjaman` int(11) NOT NULL,
  `angsuran_ke` int(2) NOT NULL,
  `tgl_pembayaran` date NOT NULL,
  `tgl_jatuh_tempo` date NOT NULL,
  `besar_angsuran` int(11) NOT NULL,
  `denda` int(11) NOT NULL,
  `jumlah_bayar` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_angsuran`),
  KEY `id_pinjaman` (`id_pinjaman`),
  CONSTRAINT `angsuran_ibfk_1` FOREIGN KEY (`id_pinjaman`) REFERENCES `pinjaman` (`id_pinjaman`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `angsuran` */

/*Table structure for table `detail_angsuran` */

DROP TABLE IF EXISTS `detail_angsuran`;

CREATE TABLE `detail_angsuran` (
  `id_detail_angsuran` int(11) NOT NULL AUTO_INCREMENT,
  `id_angsuran` int(11) NOT NULL,
  `tgl_pembayaran` date NOT NULL,
  `tgl_jatuh_tempo` date NOT NULL,
  `besar_angsuran` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_detail_angsuran`),
  UNIQUE KEY `id_angsuran` (`id_angsuran`),
  CONSTRAINT `detail_angsuran_ibfk_1` FOREIGN KEY (`id_angsuran`) REFERENCES `angsuran` (`id_angsuran`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `detail_angsuran` */

/*Table structure for table `jenis_simpanan` */

DROP TABLE IF EXISTS `jenis_simpanan`;

CREATE TABLE `jenis_simpanan` (
  `id_jenis` int(11) NOT NULL AUTO_INCREMENT,
  `nama_simpanan` varchar(255) NOT NULL,
  `besar_simpanan` varchar(255) NOT NULL,
  `type` varchar(20) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `jenis_simpanan` */

/*Table structure for table `kategori_pinjaman` */

DROP TABLE IF EXISTS `kategori_pinjaman`;

CREATE TABLE `kategori_pinjaman` (
  `id_kategori` int(11) NOT NULL AUTO_INCREMENT,
  `nama_kategori` varchar(255) NOT NULL,
  `jumlah_bulan` int(11) NOT NULL,
  PRIMARY KEY (`id_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `kategori_pinjaman` */

/*Table structure for table `petugas` */

DROP TABLE IF EXISTS `petugas`;

CREATE TABLE `petugas` (
  `id_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) NOT NULL,
  `alamat` varchar(255) NOT NULL,
  `no_telp` varchar(255) NOT NULL,
  `tempat_lahir` varchar(255) NOT NULL,
  `tanggal_lahir` date NOT NULL,
  `j_kelamin` varchar(10) NOT NULL,
  `keterangan` text,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id_petugas`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

/*Data for the table `petugas` */

insert  into `petugas`(`id_petugas`,`nama`,`alamat`,`no_telp`,`tempat_lahir`,`tanggal_lahir`,`j_kelamin`,`keterangan`,`username`,`password`) values (1,'sholeh','Cimahi','028392382993829','Bandung','1994-02-09','pria','asdsadsada','sholeh','cfa85510a50ed2ef052b251239e356ea');

/*Table structure for table `pinjaman` */

DROP TABLE IF EXISTS `pinjaman`;

CREATE TABLE `pinjaman` (
  `id_pinjaman` int(11) NOT NULL AUTO_INCREMENT,
  `id_kategori` int(11) NOT NULL,
  `id_anggota` int(11) NOT NULL,
  `besar_pinjaman` double NOT NULL,
  `tgl_pengajuan` date NOT NULL,
  `tgl_acc_pengajuan` date NOT NULL,
  `tgl_pinjam` date NOT NULL,
  `tgl_pelunasan` date NOT NULL,
  `status` varchar(20) NOT NULL,
  `bunga` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_pinjaman`),
  KEY `id_anggota` (`id_anggota`),
  KEY `pinjaman_ibfk_2` (`id_kategori`),
  CONSTRAINT `pinjaman_ibfk_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id_anggota`),
  CONSTRAINT `pinjaman_ibfk_2` FOREIGN KEY (`id_kategori`) REFERENCES `kategori_pinjaman` (`id_kategori`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `pinjaman` */

/*Table structure for table `shu` */

DROP TABLE IF EXISTS `shu`;

CREATE TABLE `shu` (
  `id_shu` int(11) NOT NULL AUTO_INCREMENT,
  `id_angsuran` int(11) NOT NULL,
  `tgl_masuk` date NOT NULL,
  `besar_shu` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  PRIMARY KEY (`id_shu`),
  KEY `id_angsuran` (`id_angsuran`),
  CONSTRAINT `shu_ibfk_1` FOREIGN KEY (`id_angsuran`) REFERENCES `angsuran` (`id_angsuran`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `shu` */

/*Table structure for table `simpanan` */

DROP TABLE IF EXISTS `simpanan`;

CREATE TABLE `simpanan` (
  `id_simpan` int(11) NOT NULL AUTO_INCREMENT,
  `id_anggota` int(11) NOT NULL,
  `tgl_simpanan` date NOT NULL,
  `id_jenis` int(11) NOT NULL,
  `besar_simpanan` int(11) NOT NULL,
  `keterangan` text NOT NULL,
  `bulan` int(2) NOT NULL,
  `tahun` int(4) NOT NULL,
  PRIMARY KEY (`id_simpan`),
  KEY `id_anggota` (`id_anggota`),
  KEY `id_jenis` (`id_jenis`),
  CONSTRAINT `simpanan_ibfk_1` FOREIGN KEY (`id_anggota`) REFERENCES `anggota` (`id_anggota`),
  CONSTRAINT `simpanan_ibfk_2` FOREIGN KEY (`id_jenis`) REFERENCES `jenis_simpanan` (`id_jenis`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `simpanan` */

/*Table structure for table `super_petugas` */

DROP TABLE IF EXISTS `super_petugas`;

CREATE TABLE `super_petugas` (
  `id_super_petugas` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  PRIMARY KEY (`id_super_petugas`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

/*Data for the table `super_petugas` */

/*Table structure for table `cek_anggota_belum_lunas` */

DROP TABLE IF EXISTS `cek_anggota_belum_lunas`;

/*!50001 DROP VIEW IF EXISTS `cek_anggota_belum_lunas` */;
/*!50001 DROP TABLE IF EXISTS `cek_anggota_belum_lunas` */;

/*!50001 CREATE TABLE  `cek_anggota_belum_lunas`(
 `belum_lunas` bigint(21) ,
 `id_anggota` int(11) ,
 `nama` varchar(255) 
)*/;

/*Table structure for table `cek_anggota_lunas` */

DROP TABLE IF EXISTS `cek_anggota_lunas`;

/*!50001 DROP VIEW IF EXISTS `cek_anggota_lunas` */;
/*!50001 DROP TABLE IF EXISTS `cek_anggota_lunas` */;

/*!50001 CREATE TABLE  `cek_anggota_lunas`(
 `lunas` bigint(21) ,
 `id_anggota` int(11) ,
 `nama` varchar(255) 
)*/;

/*View structure for view cek_anggota_belum_lunas */

/*!50001 DROP TABLE IF EXISTS `cek_anggota_belum_lunas` */;
/*!50001 DROP VIEW IF EXISTS `cek_anggota_belum_lunas` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cek_anggota_belum_lunas` AS select count(if(((`pinjaman`.`status` = 'Acc') or (`pinjaman`.`status` = 'Terima')),1,NULL)) AS `belum_lunas`,`anggota`.`id_anggota` AS `id_anggota`,`anggota`.`nama` AS `nama` from (`anggota` left join `pinjaman` on((`anggota`.`id_anggota` = `pinjaman`.`id_anggota`))) group by `pinjaman`.`id_anggota` */;

/*View structure for view cek_anggota_lunas */

/*!50001 DROP TABLE IF EXISTS `cek_anggota_lunas` */;
/*!50001 DROP VIEW IF EXISTS `cek_anggota_lunas` */;

/*!50001 CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `cek_anggota_lunas` AS select count(if((`pinjaman`.`status` = 'Lunas'),1,NULL)) AS `lunas`,`anggota`.`id_anggota` AS `id_anggota`,`anggota`.`nama` AS `nama` from (`anggota` left join `pinjaman` on((`anggota`.`id_anggota` = `pinjaman`.`id_anggota`))) group by `pinjaman`.`id_anggota` */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
