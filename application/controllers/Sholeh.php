<?php

class Sholeh extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'sholeh/');
        $this->data = array();
        init_generic_dao();
        $this->load->model(array('m_sholeh','m_sholeh'));
        $this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "sholeh";
    }

    private function validate() {
        $this->form_validation->set_rules('last_name', 'Last Name', 'trim|required|max_length[50]');
        $this->form_validation->set_rules('dept_id', 'Dept ID', 'trim|required|max_length[10]|integer');
        $this->form_validation->set_rules('salary', 'Salary', 'trim|required|max_length[10]|integer');
        $this->form_validation->set_rules('job_id', 'Job ID', 'trim|required|max_length[10]|integer');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array(
            'filter_sholeh' => array(
                'last_name' => '',
                'dept_id' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->data['offset'] = $offset;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $this->data['sholeh'] = $this->m_sholeh->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['sholeh'] = $this->m_sholeh->fetch($limit, $offset, null, true, null, null, $key);
        $this->data['total_rows'] = $this->m_sholeh->fetch(null, null, null, true, null, null, $key, true);
    }

    private function fetch_input() {
        $data = array('last_name' => $this->input->post('last_name'),
            'dept_id' => $this->input->post('dept_id'),
            'salary' => $this->input->post('salary'));

        return $data;
    }

    private function insert_menu($menu_id, $employee_id, $edit) {
        foreach ($menu_id as $id) {
            $menu = array(
                'emplo_id_id' => $employee_id,
                'menu_id' => $id
            );
            $this->m_sholeh->insert($menu);
        }
    }

    public function add() {
        $obj = $this->fetch_input();

        if ($this->validate() != false) {
			
            $this->m_sholeh->insert($obj);
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['sholeh'] = (object) $obj;
            $this->template_admin->display('sholeh/sholeh_insert', $this->data);
        }
    }

    public function allmenu(){
        $result = $this->m_menu->get_whole_menu(null);
        $i = 0;
        foreach ($result as $menu) {
            $obj_id = $menu->menu_id;
            $child = $this->m_menu->get_whole_menu($obj_id);
            $result[$i]->submenu = $child;
            $j = 0;
            foreach ($child as $submenu) {
                $grandchild = $this->m_menu->get_whole_menu($submenu->menu_id);
                $result[$i]->submenu[$j]->subsubmenu = $grandchild;
                $j++;
            }
            $i++;
        }
        $this->data['menu'] = $result;
    }

    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($employee_id) {
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_at'] = date('Y-m-d H:i:s');

        $obj_id = array('employee_id' => $employee_id);
        $id = $employee_id;
        $this->menu_id($id);
        $parent = $this->input->post('parent');
        $child = $this->input->post('child');
        $grandchild = $this->input->post('grandchild');

        if ($this->validate() != false) {
            $this->m_sholeh->update($obj, $obj_id);
            $this->m_sholeh->delete($obj_id);

            //insert menu
            if(!empty($parent)){ $this->insert_menu($parent, $employee_id, true); }
            if(!empty($child)){ $this->insert_menu($child, $employee_id, true); }
            if(!empty($grandchild)){ $this->insert_menu($grandchild, $employee_id, true); }

            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->allmenu();
            $this->fetch_record($obj_id);
            $this->template_admin->display('sholeh/sholeh_insert', $this->data);
        }
    }

    public function menu_id($employee_id){
        $result = $this->m_menu->get_menu($employee_id);
        $arr = array();
        foreach ($result as $r){
            array_push($arr, $r->menu_id);
        }
        $this->data['active_menu'] = $arr;
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($employee_id) {
        $obj_id = array('employee_id' => $employee_id);

        $this->preload();
        $this->fetch_record($obj_id);
        $this->menu_sholeh($employee_id);

        $this->template_admin->display('sholeh/sholeh_detail', $this->data);
    }

    public function menu_sholeh($employee_id) {
            $menu_result = $this->m_menu->get_active_menu($employee_id, null, false);
            $i = 0;
            foreach ($menu_result as $menu) {
                $obj_id = $menu->menu_id;
                $child = $this->m_menu->get_active_menu($employee_id, $obj_id, false);
                $menu_result[$i]->submenu = $child;

                $i++;
            }
            
            $menu_master = $this->m_menu->get_active_menu($employee_id, null, true);
            $i = 0;
            foreach ($menu_master as $menu) {
                $obj_id = $menu->menu_id;
                $child = $this->m_menu->get_active_menu($employee_id, $obj_id, true);
                $menu_master[$i]->submenu = $child;
                
                $j = 1;
                foreach ($child as $submenu) {
                    $grandchild = $this->m_menu->get_active_menu($employee_id, $submenu->menu_id, true);
                    
                    if(!empty($grandchild[0])){
                        $menu_master[$i]->submenu[$j]->subsubmenu = $grandchild;
                        $j++;
                    }
                }
                $i++;
   
            }
           
            $result = array_merge((array) $menu_result, (array) $menu_master);
            $this->data['menu'] = $result;

    }

    public function delete($employee_id) {
        $obj_id = array('employee_id' => $employee_id);
        $obj = array('updated_by' => $this->session->userdata('username'), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1);
        $this->m_sholeh->update($obj, $obj_id);
        $this->session->set_flashdata(array('message' => 'Data berhasil dihapus.', 'type_message' => 'success'));
        redirect(CURRENT_CONTEXT);
    }

    public function delete_multiple() {
        $data = file_get_contents('php://input');
        $id = json_decode($data);
        $obj = array('updated_by' => $this->session->userdata('username'), 'updated_at' => date('Y-m-d H:i:s'),'is_deleted' => 1);
        foreach ($id->ids as $id) {
            $obj_id = array('employee_id' => $id->employee_id);
            $this->m_sholeh->update($obj,$obj_id);
        }
        $this->session->set_flashdata(array('message' => 'Data berhasil dihapus.', 'type_message' => 'success'));
        echo json_encode(array('status' => 200));
    }

    public function search($page = 1) {
        $this->preload();
        $key = $this->session->userdata('filter_sholeh');
        if ($this->input->post('search')) {
            $key = array(
                'last_name' => $this->input->post('last_name'),
                'dept_id' => $this->input->post('dept_id')
            );
            $this->session->set_userdata(array('filter_sholeh' => $key));
        }
        $offset = ($page - 1) * $this->limit;
        $this->data['offset'] = $offset;
        $this->get_list($this->limit, $offset, $key);
    }

    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key)) ? 'search' : 'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 3;
        $this->pagination->initialize($config);
        $this->data['pagination'] = $this->pagination->create_links();
        $this->template_admin->display('sholeh/sholeh_list', $this->data);
    }

    function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "auth");
        }
    }

}

?>