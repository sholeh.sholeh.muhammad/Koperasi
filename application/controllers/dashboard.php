<?php if (! defined('BASEPATH')) exit('No direct script access allowed'); 
class Dashboard extends CI_Controller { 
    function __construct()
    { 
        parent::__construct(); 
    }
    function index() 
    { 

    } 

    function petugas() 
    { 
        $data['title'] = 'Petugas';
        $data['page'] = 'dashboard/petugas';
        $this->load->view('template',$data); 
    } 

    function member() 
    { 
        $data['title'] = 'Member';
        $data['page'] = 'dashboard/member';
        $this->load->view('template',$data);
    } 
} 