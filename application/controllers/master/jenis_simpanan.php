<?php

class Jenis_simpanan extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', site_url() . 'master/jenis_simpanan/');
        $this->data = array();
        init_generic_dao();
        $this->load->model('m_jenis_simpanan','m_jenis');
        
        /*$this->load->library(array('template_admin'));
        $this->logged_in();
        $this->data['page_title'] = "Product Category";
        $this->path = "upload/cat_image/";
        */
        date_default_timezone_set("Asia/Jakarta");
    }

    private function validate() {
			$this->form_validation->set_rules('cat_name', 'Cat Name', 'trim|required|max_length[100]');
            $this->form_validation->set_rules('cat_status', 'Cat Status', 'trim|required|max_length[3]|integer');
            $this->form_validation->set_rules('cat_meta_title', 'Cat Meta Name', 'trim|max_length[100]');


        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
		$this->data['product_category_list'] = $this->m_jenis->fetch();
    }

    public function index($page = 1,$id_jenis = null, $subid_jenis = null) {
        $this->preload();
        $this->session->set_userdata(array('filter_product_category' => array(
				'nama_simpanan' => ''))
        );
        $key = array();
        $this->data['id_jenis'] = $id_jenis;
        $this->data['subid_jenis'] = $subid_jenis;
        if(!empty($id_jenis)){
            $key = array('id_jenis'=>(($subid_jenis)?:$id_jenis));
            $this->data['back_url'] = $this->data['current_context']."index/1/".(($subid_jenis)?$id_jenis:'');
            //$this->data['category'] = $this->m_jenis->by_id(array('id_jenis'=>(($subid_jenis)?:$id_jenis)));
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }

    public function fetch_record($keys) {
        $this->data['product_category'] = $this->m_jenis->by_id($keys);
    }

    private function fetch_data($limit, $offset, $key) {
        $this->data['product_category'] = $this->m_jenis->fetch($limit, $offset, 'id_jenis', true,null, $key);

        $this->data['total_rows'] = $this->m_jenis->fetch(null,null, null, true,null, $key, null,true);

        $this->data['coba'] = "test123";
    }

    private function fetch_input() {

        $data = array(
			'cat_name' => $this->input->post('cat_name'),
			'cat_status' => $this->input->post('cat_status'),
            'cat_permalink'=> $this->input->post('cat_permalink'),
            'id_jenis' => $this->input->post('id_jenis'));

        return $data;
    }

    public function add($parent1= null, $parent2= null) {
        $obj = $this->fetch_input();
        $obj['created_by'] = $this->session->userdata('username');
        $obj['created_on'] = date('Y-m-d H:i:s');
        if ($this->validate() != false) {
            $this->m_jenis->insert($obj);
            $this->session->set_flashdata(array('message'=>'Data inserted successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $this->data['product_category'] = (object) $obj;
            $this->data['page'] = 'master/jenis-simpanan/FORM';
            $this->data['title'] = 'Master jenis simpanan insert';
            $this->load->view('template', $this->data);
//            $this->template_admin->display('admin/product_category/product_category_insert', $this->data);
        }
    }

    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($id_jenis) {
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('id_jenis' => $id_jenis);
  
        if ($this->validate() != false) {
            // if(!empty($_FILES['image']['name'])){
            //     $path = "upload/cat_image/";
            //     $imageName = $path.(md5(uniqid(rand(),true))).".png";
            //     $obj['cat_image'] = $imageName;
            //     $tempFile = $_FILES['image']['tmp_name'];
            //     move_uploaded_file($tempFile, $imageName);;
            //     @unlink("./".$this->input->post('image_old')); 
            // }
            $this->m_jenis->update($obj, $obj_id);
            // $p = $this->m_jenis->by_id(array('id_jenis'=>$id_jenis));
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            //$this->template_admin->display('admin/product_category/product_category_insert', $this->data);
        }
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($id_jenis) {
        $obj_id = array('id_jenis' => $id_jenis);

        $this->preload();
        $this->fetch_record($obj_id);
        //$this->template_admin->display('product_category/product_category_detail', $this->data);
    }

    public function delete($id_jenis, $cat = null, $subid_jenis = null) {

        $parent = array('id_jenis' => $id_jenis);
        $parent1 = $this->m_jenis->fetch(null,null,null,true,null,$parent);
        if(!empty($parent1)){
            foreach ($parent1 as $p1) {
                $parent = array('id_jenis' => $p1->id_jenis);
                $parent2 = $this->m_jenis->fetch(null,null,null,true,null,$parent);
                if(!empty($parent2)){
                    foreach ($parent2 as $p2) {
                        $id = array('id_jenis'=>$p2->id_jenis);
                        $pr = $this->m_product->by_id($id);
                        if(!empty($pr)){
                            $this->m_product->update(array('id_jenis'=>0),$id);
                        }
                        $this->m_jenis->delete($id);
                    }
                }
                $id = array('id_jenis'=>$p1->id_jenis);
                $pr = $this->m_product->by_id($id);
                if(!empty($pr)){
                    $this->m_product->update(array('id_jenis'=>0),$id);
                }
                $this->m_jenis->delete($id);
            }
        }
        $obj_id = array('id_jenis' => $id_jenis);
        $pr = $this->m_product->by_id($obj_id);
        if(!empty($pr)){
            $this->m_product->update(array('id_jenis'=>0),$obj_id);
        }
        
        $this->m_jenis->delete($obj_id);
        $this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));

        redirect(CURRENT_CONTEXT."index/1/".(($cat)?"$cat/":"").(($subid_jenis)?"$subid_jenis/":""));
    }
	
	public function delete_multiple(){

        $data = file_get_contents('php://input');
        $ids = json_decode($data);
        //$input=$this->input->post('ids');        
        //foreach($input as $id){\-r9
        foreach($ids->ids as $id){
            //$obj_id = array('id_jenis' => $id['id_jenis']);
            $parent = array('id_jenis' => $id->id_jenis);
            $parent1 = $this->m_jenis->fetch(null,null,null,true,null,$parent);
            if(!empty($parent1)){
                foreach ($parent1 as $p1) {
                    $parent = array('id_jenis' => $p1->id_jenis);
                    $parent2 = $this->m_jenis->fetch(null,null,null,true,null,$parent);
                    if(!empty($parent2)){
                        foreach ($parent2 as $p2) {
                            $id = array('id_jenis'=>$p2->id_jenis);
                            $pr = $this->m_product->by_id($id);
                            if(!empty($pr)){
                                $this->m_product->update(array('id_jenis'=>0),$id);
                            }
                            $this->m_jenis->delete($id);
                        }
                    }
                    $id = array('id_jenis'=>$p1->id_jenis);
                    $pr = $this->m_product->by_id($id);
                    if(!empty($pr)){
                        $this->m_product->update(array('id_jenis'=>0),$id);
                    }
                    $this->m_jenis->delete($id);
                }
            }
            $obj_id = array('id_jenis' => $id->id_jenis);
            $pr = $this->m_product->by_id($obj_id);
            if(!empty($pr)){
                $this->m_product->update(array('id_jenis'=>0),$obj_id);
            }
            
            $this->m_jenis->delete($obj_id);
            
        }
        $this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        echo json_encode(array('status'=>200));
    }

	public function search($page = 1) {
        $this->preload();
		$key = $this->session->userdata('filter_product_category');
        if ($this->input->post('search')) {
            $key = array(
				'nama_simpanan' => $this->input->post('nama_simpanan')
            );
			$this->session->set_userdata(array('filter_product_category' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
    }
	
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination
        $this->fetch_data($limit, $offset, $key);
        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config);
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();
        $this->data['page'] = 'master/jenis-simpanan/index';
        $this->data['title'] = 'Master jenis simpanan';
        $this->load->view('template', $this->data);
    }

    function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin/auth");
        }
    }

}

?>