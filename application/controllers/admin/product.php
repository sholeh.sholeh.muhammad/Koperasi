<?php

class Product extends CI_Controller {

    public $data;
    public $filter;
    public $limit = 10;

    public function __construct() {
        parent::__construct();
        define('CURRENT_CONTEXT', base_url() . 'admin/product/');
        $this->data = array();//data sebagai array
        init_generic_dao();
        $this->load->model(array('m_product','m_product_category','m_product_photo'));
        $this->load->library(array('template_admin','cart','image_lib'));
        $this->logged_in();
        $this->data['page_title'] = "Product";
        date_default_timezone_set("Asia/Jakarta");
    }

    private function validate() {			
            // $this->form_validation->set_rules('cat_id', 'Cat Id', 'trim|required|max_length[10]|integer');
			$this->form_validation->set_rules('product_name', 'Product Name', 'trim|required|max_length[150]');
			// $this->form_validation->set_rules('product_description', 'Product Description', 'trim|required');
			// $this->form_validation->set_rules('product_price', 'Product Price', 'trim|required|max_length[12]|numeric');
			// $this->form_validation->set_rules('product_stock', 'Product Stock', 'trim|max_length[10]|integer');
			// $this->form_validation->set_rules('product_ispreorder', 'Product Ispreorder', 'trim|required|max_length[3]|integer');
			// $this->form_validation->set_rules('product_preorder_day', 'Product Preorder Day', 'trim|max_length[10]|integer');

			// $this->form_validation->set_rules('product_discount', 'Product Discount', 'trim|max_length[12]|numeric');
			// $this->form_validation->set_rules('product_ispercent', 'Product Ispercent', 'trim|max_length[3]|integer');
			//$this->form_validation->set_rules('product_isbestseller', 'Product Isbestseller', 'trim|max_length[3]|integer');
			//$this->form_validation->set_rules('product_condition', 'Product Condition', 'trim|max_length[1]');
			
            // $this->form_validation->set_rules('product_weight', 'Product Weight', 'trim|required|max_length[12]|numeric');

			//$this->form_validation->set_rules('product_minorder', 'Product Minorder', 'trim|required|max_length[10]|integer');
			// $this->form_validation->set_rules('product_status', 'Product Status', 'trim|required|max_length[3]|integer');
   //          $this->form_validation->set_rules('product_ishotsale', 'Product Hot Sale', 'trim|required|max_length[3]|integer');
   //          $this->form_validation->set_rules('product_isnewarrival', 'Product New Arrival', 'trim|required|max_length[3]|integer');
   //          $this->form_validation->set_rules('product_ispopular', 'Product Popular', 'trim|required|max_length[3]|integer');
            
            // $this->form_validation->set_rules('product_material', 'Product Material', 'trim|required');

        return $this->form_validation->run();
    }

    /**
      prepare data for view
     */
    public function preload() {
        $this->data['current_context'] = CURRENT_CONTEXT;
        $pc = array();
        $this->m_product_category->or_where(array(array('parent_cat_id'=>NULL)));
		$cat = $this->m_product_category->fetch();
        foreach ($cat as $c1) {
            array_push($pc, $c1);
            $cat2 = $this->m_product_category->fetch(null,null,null,true,null,array('parent_cat_id'=>$c1->cat_id));
            foreach ($cat2 as $c2) {
                $c2->cat_name = "&nbsp;&nbsp;&nbsp;&nbsp;".$c2->cat_name;
                array_push($pc, $c2);
                $cat3 = $this->m_product_category->fetch(null,null,null,true,null,array('parent_cat_id'=>$c2->cat_id));
                foreach ($cat3 as $c3) {
                    $c3->cat_name = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$c3->cat_name;
                    array_push($pc, $c3);
                }
            }
        }
         $this->data['product_category_list'] = $pc;
    }

    public function index($page = 1) {
        $this->preload();
        $this->session->set_userdata(array(
            'filter_product' => array(
				'product_name' => '',
                'cat_id' => '',
                'product_stock' => '',
                'product_status' => '',
                'product_discount' => '',
                'product_sold' => ''))
        );
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset);
    }

    public function fetch_record($keys) {
        $product = $this->m_product->by_id($keys);
        $attr = $this->m_product_attr->fetch(null,null,null,true,null,$keys);
        $photos = $this->m_product_photo->fetch(null,null,null,true,null,$keys);
        $var = $this->m_varian->fetch(null, null, null, true, null, $keys);
        $variant = array();
        foreach($var as $row){
            $option = $this->m_varian_options->fetch(null, null, null, true, null, array('product_id' => $keys['product_id'], 'varian_name' => $row->varian_name));
            array_push($variant, (object) array_merge((array) $row, array('options' => $option)));
        }
        $this->data['product'] = (object) array_merge((array) $product, array('variant' => $variant, 'photos' => $photos, 'attr' => $attr));
        // echo "<pre>";print_r($this->data['product']);die();
    }

    //banyak data dengan kondisi
    private function fetch_data($limit, $offset, $key) {
        $condition = "";
        // if(!empty($key)){
        //     $condition = ($key['product_status'])?" product_status = $key[product_status]":"";
        //     $condition .= (($key['product_name'])?(($condition)?" and ":"")." product_name like '%$key[product_name]%'":"");
        //     $condition .= (($key['product_sold'])?(($condition)?" and ":"")." product_sold >= $key[product_sold]":"");
        //     $condition .= (($key['product_discount'])?(($condition)?" and ":"")." discount >= '$key[product_discount]'":"");
        //     $condition .= (($key['cat_id'])?(($condition)?" and ":"")." cat_id = '$key[cat_id]'":"");
        //     $condition .= (($key['product_stock'])?(($condition)?" and ":"")." product_stock >= '$key[product_stock]'":"");
        // }

        // $products = $this->m_product->fetch();
        // $arr = array();
        // foreach ($products['data'] as $key => $value) {
        //     $cek = $this->m_order_product->by_id(array('product_id' => $value->product_id));
        //     $a = (!empty($cek)?'ada':'kosong');
        //     array_push($arr, $a);
        // }
        // $this->data['is_ordered'] = $arr;
        // $this->data['total_rows'] = 20;
        // $this->data['product'] = $products;

        $this->data['product'] = $this->m_product->fetch($limit, $offset, null, true,null, null, $key);
        $this->data['total_rows'] = $this->m_product->fetch(null,null, null, true,null, null, $key,true);
        // echo "<pre>";
        // print_r($this->data['product']);die();
        // if($key['product_status']==2){
        //     $key['product_status'] = null;      
        //     $exact['product_status'] = (!is_null($key['product_status'])? $key['product_status']:'');
        //     $like['product_name'] = (!is_null($key['product_name'])? $key['product_name']:'');     
        //     $exact['product_sold'] = (!is_null($key['product_sold'])? $key['product_sold']:'');
        //     $exact['product_discount'] = (!is_null($key['product_discount'])? $key['product_discount']:'');
        //     $exact['cat_id'] = (!is_null($key['cat_id'])?$key['cat_id']:'');
        //     $exact['product_stock'] = (is_null($key['product_stock'])? '' : $key['product_stock']);
        // }else{
        //     $exact['product_status'] = (!is_null($key['product_status'])? $key['product_status']:'');
        //     $like['product_name'] = (!is_null($key['product_name'])? $key['product_name']:'');     
        //     $exact['product_sold'] = (!is_null($key['product_sold'])? $key['product_sold']:'');
        //     $exact['product_discount'] = (!is_null($key['product_discount'])? $key['product_discount']:'');
        //     $exact['cat_id'] = (!is_null($key['cat_id'])?$key['cat_id']:'');
        //     $exact['product_stock'] = (is_null($key['product_stock'])? '' : $key['product_stock']);
        // }

        // $this->data['product'] = array();
        // $product = $this->m_product->fetch($limit, $offset, null, true, $like, $exact);           
        // foreach ($product as $row) {
        //     $varian = $this->m_product_attr->fetch(null, null, 'attr_price', true, null, array('product_id' => $row->product_id));
        //     if(!empty($varian)){
        //         if($varian[0]->attr_price != $varian[sizeof($varian) - 1]->attr_price){
        //             $row->product_price = "Rp ".number_format($varian[0]->attr_price)." - Rp".number_format($varian[sizeof($varian) - 1]->attr_price);    
        //         }else{
        //             $row->product_price = "Rp ".number_format($varian[0]->attr_price);
        //         }
        //         if($varian[0]->attr_discount != $varian[sizeof($varian) - 1]->attr_discount){
        //             $row->product_discount = ($varian[0]->attr_discount)."% - ".($varian[sizeof($varian) - 1]->attr_discount)."%";    
        //         }else{
        //             $row->product_discount = ($varian[0]->attr_discount)."%";
        //         }
        //         $stock = 0;
        //         foreach($varian as $var){
        //             $stock += $var->attr_stock; 
        //         }
        //         $row->product_stock = $stock;
        //     }else{
        //         $row->product_price = "Rp ".number_format($row->product_price);
        //     }
        //     array_push($this->data['product'], $row);
        // }

        // $this->data['total_rows'] = $this->m_product->fetch(null,null, null, true,$like, $exact,null,true);      
    }

    private function fetch_input() {
        //Create Permalink
        // print_r($this->input->post('product_name'));die();
        // $permalink = url_title($this->input->post('product_name'));
        // print_r($permalink);die();
        // $count_permalink = $this->m_product->count_all(array('product_permalink'=>$permalink));
        // $permalink = ($count_permalink > 0)?$permalink."-".($count_permalink+1):$permalink;

        //Input data
        $data = array('cat_id' => $this->input->post('cat_id'),
			'product_name' => $this->input->post('product_name'),
			'product_description' => $this->input->post('product_description'),
			'product_price' => $this->input->post('product_price'),
			'product_stock' => $this->input->post('product_stock'),
			// 'product_ispreorder' => $this->input->post('product_ispreorder'),
			// 'product_preorder_day' => $this->input->post('product_preorder_day'),
			'product_discount' => $this->input->post('product_discount'),
			// 'product_minorder' => $this->input->post('product_minorder'),
            // 'product_ispercent' => $this->input->post('product_ispercent'),
			//'product_isbestseller' => $this->input->post('product_isbestseller'),
			// 'product_seen' => 0,
			// 'product_sold' => 0,
			// 'product_condition' => $this->input->post('product_condition'),
			'product_weight' => $this->input->post('product_weight'),
			// 'product_minorder' => $this->input->post('product_minorder'),
            // 'product_permalink' => $permalink,
			'product_status' => $this->input->post('product_status'));
            // 'product_ishotsale' => $this->input->post('product_ishotsale'),
            // 'product_isnewarrival' => $this->input->post('product_isnewarrival'),
            // 'product_ispopular' => $this->input->post('product_ispopular'),
            // 'product_material' =>$this->input->post('product_material'),
            // 'product_metaname'=>$this->input->post('product_metaname'),
            // 'product_metadescription' => $this->input->post('product_metadescription'));

        return $data;
    }

    public function add() {
        $obj = $this->fetch_input();
        $obj['created_by'] = $this->session->userdata('username');//audit trail
        $obj['created_on'] = date('Y-m-d H:i:s');//audit trail

        if ($this->validate() != false) {
            $this->m_product->insert($obj);
            $pid = $this->db->insert_id();
            $this->save_image($pid);
            $this->session->set_flashdata(array('message'=>'Data inserted successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT);
        } else {
            $this->preload();
            $this->data['edit'] = false;
            #set value
            $dt = date("Y/m/d h:i:sa");
            $this->data['token'] = md5($dt);
            $this->data['product'] = (object) $obj;
            $this->template_admin->display('admin/product/product_insert', $this->data);
        }
        
    }

    private function reArrayFiles(&$file_post) {

        $file_ary = array();
        $file_count = count($file_post['name']);
        $file_keys = array_keys($file_post);
        // print_r($file_post);die();
        for ($i=0; $i<$file_count; $i++) {
            foreach ($file_keys as $key) {
                $file_ary[$i][$key] = $file_post[$key][$i];
            }
        }

        return $file_ary;
    }

    public function save_temp_image(){
        $this->load->helper(array('file','url'));
        $config['upload_path']   = 'upload/prod_photo/temp/';
        $config['allowed_types'] = 'gif|jpg|png|ico';
        $config['file_name'] = json_decode($this->input->post('name'));
        $this->load->library('upload', $config);
        // $images = $this->reArrayFiles($_FILES['imagesend']);
// print_r($_FILES['imagesend']);die();
        foreach ($images as $key => $image) {
                $unique_name = "upload/prod_photo/temp/".$config['file_name'][$key];
                move_uploaded_file($image['tmp_name'], $unique_name);
            }
            echo "200";
    }

    private function save_image($productID){
        $targetPath = 'upload/prod_photo/';
        $info_logo = $targetPath.(md5(uniqid(rand(),true))).".png";
        $obj['prodphoto_path'] = $info_logo;
        $tempFile = $_FILES['product_photo']['tmp_name'];
        move_uploaded_file($tempFile, $info_logo);
        $obj['product_id'] = $productID;
        $this->m_product_photo->insert($obj);
        // @unlink("./".$this->input->post('info_logo_old'));

    }

    private function save_images(){
        //saving image
        $this->load->helper(array('file'));
        $productId = 1;
        $images = $this->input->post('images');
        // print_r($images);
        // exit();
        $prodphoto_description = $this->input->post('prodphoto_description');
        $prodphoto_isprimary = $this->input->post('is_primary');
        if(!empty($images)){
            $path = "upload/prod_photo/$productId/";
            @mkdir("./".$path, $mode = 0777, true);
            $this->m_product_photo->delete(array('product_id'=>$productId));
            $i = 0;
            $ch = [];
            foreach ($images as $image) {
                $unique_name = $path.(md5(uniqid(rand(),true))).".png";
                copy("./".$image, "./".$unique_name);
                $arr = array(
                    'product_id' => $productId,
                    'prodphoto_path' => $unique_name,
                    'prodphoto_isprimary' => (!empty($prodphoto_isprimary[$i])?'1':'0'),
                    'prodphoto_description' => $prodphoto_description[$i],
                    'created_by' => $this->session->userdata('username'),
                    'created_on' => date('Y-m-d H:i:s')
                );
                array_push($ch, $arr);
                // $this->m_product_photo->insert($arr);
                @unlink("./".$image);
                $i++;
            }
            $this->debug($ch);
        }
    }

    private function save_variant($productId){
        $available = $this->input->post('product_variant_available');
        // $ch = [];
        $stock = 0;
        if (!empty($available)) {
            foreach ($available as $key => $val) {
                $variant = array('product_id' => $productId,
                                'attr_varian' => $this->input->post('product_variant_name')[$key],
                                'attr_price' => $this->input->post('product_variant_price')[$key],
                                'attr_discount' => $this->input->post('product_variant_discount')[$key],
                                'attr_stock' => $this->input->post('product_variant_stock')[$key]
                                );
                $stock += $variant['attr_stock'];
                $this->m_product_attr->insert($variant);
                // array_push($ch, $variant);
            }
            $this->m_product->update(array('product_stock' => $stock), array('product_id' => $productId));
        }
        // $this->debug($ch);
    }

    private function save_attr($productId){
        $attr = $this->input->post('attr_name');
        // $cho = [];
        if (!empty($attr)) {
            $i = 1;
            foreach ($attr as $key => $value) {
                if (!empty($value)) {
                    $obj = array('var_id' => $i, 'product_id' => $productId,'varian_name' => $value);
                    $this->m_varian->insert($obj);
                    // array_push($cho, $value);
                    $this->save_attr_options($obj, $key);
                    $i++;
                }
            }
        }
        // echo "<pre>";
        // print_r($cho);
    }

    private function save_attr_options($obj, $key){
        $opt = $this->input->post('options')[$key];
        // $cha = [];
        if (!empty($opt)) {
            foreach ($opt as $value) {
                if (!empty($value)) {
                    $obj['varian_option'] = $value;
                    unset($obj['var_id']);
                    $this->m_varian_options->insert($obj);
                    // array_push($cha, $value);
                }
            }
        }
        // echo "<pre>";
        // print_r($cha);
    }
    /**

      @description
      viewing editing form. repopulation for every data needed in form done here.
     */
    public function edit($product_id) {
        // print_r($product_id);die();
        $obj = $this->fetch_input();
        $obj['updated_by'] = $this->session->userdata('username');
        $obj['updated_on'] = date('Y-m-d H:i:s');

        $obj_id = array('product_id' => $product_id);
        // echo "<pre>";print_r($obj);die();
        if ($this->validate() != false) {
            $this->m_product->update($obj, $obj_id);
            
            $this->m_varian_options->delete($obj_id);
            $this->m_varian->delete($obj_id);
            $this->m_product_attr->delete($obj_id);
            $this->save_attr($product_id);
            $this->save_variant($product_id);
            
            $images = $this->input->post('path_image_temp');
            if(!empty($images)){
                $this->m_product_photo->delete($obj_id);
                $this->save_image($product_id);    
            }

            $status = $this->uri->segment(5);
            $page = $this->uri->segment(6);
            $this->session->set_flashdata(array('message'=>'Data edited successfully.','type_message'=>'success'));
            redirect(CURRENT_CONTEXT . $status .'/'. $page);
        } else {
            $this->preload();
            $dt = date("Y/m/d h:i:sa");
            $this->data['token'] = md5($dt);
            $this->data['edit'] = true;
            $this->fetch_record($obj_id);
            $this->template_admin->display('product/product_insert', $this->data);
        }
    }

    /**
      @description
      viewing record. repopulation for every data needed for view.
     */
    public function detail($product_id) {
        $obj_id = array('product_id' => $product_id);
        $this->preload();
        $this->fetch_record($obj_id);
        $this->template_admin->display('product/product_detail', $this->data);
    }

    public function delete($product_id) {
        $obj_id = array('product_id' => $product_id);
        $old = $this->m_product_photo->by_id($obj_id);
        @unlink("./".$old->prodphoto_path);
        if(empty($cek)){
            $this->m_varian_options->delete($obj_id);
            $this->m_varian->delete($obj_id);
            $this->m_product_attr->delete($obj_id);
            $this->m_product_review->delete($obj_id);
            $this->m_product_photo->delete($obj_id);
            $this->m_wishlist->delete($obj_id);
            $this->m_promo_product->delete($obj_id);            
            $this->m_product->delete($obj_id);
		  $this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
        }else{
          $this->session->set_flashdata(array('message'=>'Data cannot be removed, because is has been ordered.','type_message'=>'error'));  
        }
        redirect(CURRENT_CONTEXT);
    }
	
	public function delete_multiple(){
        $data = file_get_contents('php://input');
        //$input=$this->input->post('ids');
        $id = json_decode($data);
		//foreach($input as $id){
        foreach($id->ids as $id){
			//$obj_id = array('product_id' => $id['product_id']);
            $obj_id = array('product_id' => $id->product_id);
            $old = $this->m_product_photo->by_id($obj_id);
            @unlink("./".$old->prodphoto_path);
            if(empty($cek)){
                $this->m_varian_options->delete($obj_id);
                $this->m_varian->delete($obj_id);
                $this->m_product_attr->delete($obj_id);
                $this->m_product_review->delete($obj_id);
                $this->m_product_photo->delete($obj_id);
                $this->m_wishlist->delete($obj_id);
                $this->m_promo_product->delete($obj_id);            
    			$this->m_product->delete($obj_id);
                $this->session->set_flashdata(array('message'=>'Data successfully removed.','type_message'=>'success'));
                echo json_encode(array('status'=>200));
            }else{
              $this->session->set_flashdata(array('message'=>'Data cannot be removed, because is has been ordered.','type_message'=>'error'));  
              echo json_encode(array('status'=>404));
            }
		}
    }

	public function search($page = 1) {
        $this->preload();
		$key = $this->session->userdata('filter_product');
        if ($this->input->post('search')) {
            $key = array(
				'product_name' => $this->input->post('product_name'),
                'cat_id' => $this->input->post('cat_id'),
                'product_stock' => $this->input->post('product_stock'),
                'product_status' => $this->input->post('product_status'),
                'product_discount' => $this->input->post('product_discount'),
                'product_sold' => $this->input->post('product_sold'),

            );
			$this->session->set_userdata(array('filter_product' => $key));  
        }
        $offset = ($page - 1) * $this->limit;
        $this->get_list($this->limit, $offset, $key);
        
         //print_r($key);
         //die();

    }
	
    public function get_list($limit = 10, $offset = 0, $key = null) {
        #generate pagination

        $this->fetch_data($limit, $offset, $key);

        $config['base_url'] = CURRENT_CONTEXT . ((!empty($key))?'search':'index');
        $config['total_rows'] = $this->data['total_rows'];
        $config['per_page'] = $limit;
        $config['uri_segment'] = 4;
        $this->pagination->initialize($config); //paging
        $this->data['offset'] = $offset;
        $this->data['pagination'] = $this->pagination->create_links();

        $this->template_admin->display('admin/product/product_list', $this->data);
    }

    public function logged_in() {
        if (!($this->session->userdata('logged_in'))) {
            redirect(base_url() . "admin/auth");
        }
    }

    public function cropPicture(){
        $param = (object) $this->input->post();
        if($_FILES['upload']){

            $temp_dir = "upload/prod_photo/temp/";

            $file = $_FILES['upload'];

            $fileid = random_string('alnum',20);
            $filename = "{$fileid}.png";

            $filepath = "{$temp_dir}{$filename}";

            if(!is_dir($temp_dir)){
                mkdir("./".$temp_dir, $mode = 0777, true);    
            }

            // move_uploaded_file($file['tmp_name'], $filepath);
            // $config['image_library'] = 'imagemagick';
            // $config['library_path'] = '/usr/X11R6/bin/';
            $config['source_image'] = $file['tmp_name'];
            $config['x_axis'] = $param->x;
            $config['y_axis'] = $param->y;
            $config['width'] = $param->w;
            $config['height'] = $param->h;
            $config['new_image'] = $filepath;
            $config['maintain_ratio'] = false;
            $this->image_lib->initialize($config);

            if ( ! $this->image_lib->crop())
            {
                    echo $this->image_lib->display_errors();
            }else{
            echo json_encode([
                        'message'=>'OK', 
                        'fileId' => $fileid,
                        'files' => [
                            'thumb' => "/$filepath",
                            'orig' => "/$filepath"
                        ]]);
            }


        }else{
            echo json_encode(['message'=>'has no file'],404);
        }
    }

    private function debug($obj){
        echo "<pre>";
        print_r($obj);
        echo "</pre>";
        die();

    }

    public function get_data() {
        $id = $this->input->post('id');
        $data['available'] = true;
        $result = $this->m_product->by_id(array('product_id'=>$id));
        $attr = $this->m_product_attr->fetch(null, null, null, true, null, array('product_id' => $id));
        if (!empty($result)) {
            if ($result->product_stock > 0) {
                $data['id'] = $id;
                $data['nama'] = $result->product_name;
                $data['harga'] = $result->product_price;
                $data['attr'] = $attr;
                $data['stock'] = $result->product_stock;
                $data['diskon'] = $result->product_discount;
            }else{
                $data['available'] = false;
            }
        } else {
            $data['available'] = false;
        }
        echo json_encode($data);
    }

    public function get_data_attr() {
        $id = $this->input->post('id');
        $product_id = $this->input->post('product_id');
        $data['available'] = true;
        $result = $this->m_product_attr->by_id(array('product_id' => $product_id, 'attr_varian' => $id));
        // print_r($this->db->last_query());die();
        if (!empty($result)) {
            if ($result->attr_stock > 0) {
                $data['id'] = $id;
                $data['harga'] = $result->attr_price;
                $data['stock'] = $result->attr_stock;
                $data['diskon'] = $result->attr_discount;
            }else{
                $data['available'] = false;
            }
        } else {
            $data['available'] = false;
        }
        echo json_encode($data);
    }

    private function fetch_input_attr() {
        

        //Input data
       // $data = array('cat_id' => $this->input->post('cat_id'),
            
            $this->input->post('product_variant_available');
            $this->input->post('product_variant_name');
            $this->input->post('product_variant_price');
            $this->input->post('product_variant_discount');
            $this->input->post('product_variant_disp_price');
            $this->input->post('product_variant_stock');

            $data = array('attr_id' => $this->input->post('cat_id'),
            'product_id' => $this->input->post('product_name')
            

            );
            

        //return $data;
    }

    public function ajax_get_collection(){
        $pc = array();
        $this->m_product_category->or_where(array(array('parent_cat_id'=>NULL)));
        $cat = $this->m_product_category->fetch();
        foreach ($cat as $c1) {
            array_push($pc, $c1);
            $cat2 = $this->m_product_category->fetch(null,null,null,true,null,array('parent_cat_id'=>$c1->cat_id));
            foreach ($cat2 as $c2) {
                $c2->cat_name = "&nbsp;&nbsp;&nbsp;&nbsp;".$c2->cat_name;
                array_push($pc, $c2);
                $cat3 = $this->m_product_category->fetch(null,null,null,true,null,array('parent_cat_id'=>$c2->cat_id));
                foreach ($cat3 as $c3) {
                    $c3->cat_name = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;".$c3->cat_name;
                    array_push($pc, $c3);
                }
            }
        }
        $data['product_category_list'] = $pc;
        $data['html_view'] = $this->load->view('admin/product_category/ajax_collection', $data, true);
        echo json_encode($data['html_view']);
    }

}

?>