<?php if (! defined('BASEPATH')) exit('No direct script access allowed'); 
class Login extends CI_Controller { 
    function __construct()
    { 
        parent::__construct(); 
        $this->load->helper(array('form'));
        $this->load->database();
        $this->load->model('login_model', 'login');
    } 
    function index() 
    { 
        if(isset($this->session->userdata['logged_in'])){
            redirect('login/home');
        }else{
            $this->load->view('auth/login');
        }
    } 

    function login_post()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|required|xss_clean');
        $this->form_validation->set_rules('password', 'Password', 'trim|required|xss_clean');
        
        $username = $this->input->post('username');
        $password = $this->input->post('password');
        
        $data = array(
            'username' => $username,
            'password' => $password
        );
        if(isset($this->session->userdata['logged_in'])){
            redirect('login/home');
        }else{
            $validasi = $this->validasi_login($username, $password);

            if(is_array($validasi) == false){
                $result = $this->login->login($data);
                $r = $result->num_rows();
                $r_data = $result->row();
                if($r > 0){
                    $session_data = array(
                        'username'  => $r_data->username,
                        'password'  => $r_data->password,
                    );
                    // Add user data in session
                    $this->session->set_userdata('logged_in', $session_data);
                    if($result->role=='member'){
                        redirect('dashboard/member');
                    }else{
                        redirect('dashboard/petugas');
                    }
                    
                }else{
                    $validasi = array('invalid'=>'Username dan password tidak ada di database');
                    $this->load->view('auth/login',$validasi);
                }

            }else{
                $this->load->view('auth/login',$validasi);
            }
        }
    }

    function home()
    {
        if(isset($this->session->userdata['logged_in'])){
            $this->load->view('home');
        }else{
            redirect('login');
        }       
    } 

    private function validasi_login($username, $password)
    {
        $data = array();
        $data['error_string'] = array();
        $data['status'] = TRUE;

        if (!preg_match("/^[A-Za-z][A-Za-z0-9]{5,31}$/",$username)) {
            $data['error_string']['username'] = 'Format Username tidak sesuai';
            $data['status'] = FALSE;
        }

        if (!preg_match("/^(?=.{8})(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])[a-zA-Z0-9\d]+$/i",$password)) {
            $data['error_string']['password'] = 'Format Password tidak sesuai';
            $data['status'] = FALSE;
        }

        if($data['status'] === FALSE)
        {
            return $data;
        }else
        {
            return true;
        }
    }

    function logout()
    {
        $sess_array = array(
            'username' => '',
            'username' => ''
        );
        $this->session->unset_userdata('logged_in', $sess_array);
        redirect('login');
    }
} 