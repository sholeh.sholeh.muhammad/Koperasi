<?php
class M_product_photo extends Generic_dao {

    public function table_name() {
        return Tables::$product_photo;
    }

    public function field_map() {
		return array(
			'product_id' => 'product_id',
			'prodphoto_path' => 'prodphoto_path',
			'prodphoto_description' => 'prodphoto_description',
			'prodphoto_isprimary' => 'prodphoto_isprimary',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

     public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$product,
                'condition' => Tables::$product . '.product_id = ' . $this->table_name() . '.product_id',
                'field' => 'product_name as product_name, prodphoto_path as prodphoto_path'
            )
        );
    }

    // function get_all_prodphoto(){
    //     $sql = "select * from product_photo";
    //     $query = $this->db->query($sql);
    //     return $query->result();
    // }
}

?>