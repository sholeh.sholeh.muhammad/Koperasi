<?php
class M_sholeh extends Generic_dao {

    public function table_name() {
        return Tables::$sholeh;
    }

    public function field_map() {
		return array(
			'employee_id' => 'employee_id',
			'last_name' => 'last_name',
			'dept_id' => 'dept_id',
            'salary' => 'salary',
            'job_id' => 'job_id'
		);
    }

    public function __construct() {
        parent::__construct();
    }

}

?>