<?php
class M_kategori_peminjaman extends Generic_dao {

    public function table_name() {
        return Tables::$kategori_pinjaman;
    }

    public function field_map() {
		return array(
			'id_kategori'      => 'id_kategori',
			'nama_kategori'    => 'nama_kategori',
			'jumlah_bulan'     => 'jumlah_bulan'
		);
    }

    public function __construct() {
        parent::__construct();
    }

    public function get_category($parent = null){
//    	$where = array('parent_cat_id'=>$parent, 'cat_status'=>1);
    	return $this->ci->db->get($this->table_name())->result();
    }

    public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$kategori_pinjaman ." as parent ",
                'condition' => 'parent.id_kategori = '.$this->table_name().'.id_kategori',
                'field' => 'parent.nama_kategori as parent_cat_name',
                'direction' => 'left'
            )
        );
    }

}

?>