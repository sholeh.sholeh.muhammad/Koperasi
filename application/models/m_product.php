<?php
class M_product extends Generic_dao {

    public function table_name() {
        return Tables::$product;
    }

    public function field_map() {
    return array(
      'product_id' => 'product_id',
      'cat_id' => 'cat_id',
      'product_name' => 'product_name',
      'product_description' => 'product_description',
      'product_price' => 'product_price',
      'product_stock' => 'product_stock',
      
      'product_discount' => 'product_discount',
      'product_weight' => 'product_weight',
      'product_status' => 'product_status',
      'created_by' => 'created_by',
      'created_on' => 'created_on',
      'updated_by' => 'updated_by',
      'updated_on' => 'updated_on',
     
      'product_material' => 'product_material',
    );
    }    

    public function __construct() {
        parent::__construct();
    }

    public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$product_photo,
                'condition' => Tables::$product_photo . '.product_id = ' . $this->table_name() . '.product_id',
                'field' => 'prodphoto_path as product_photo',
                'direction' => 'left'
            ),
            array(
                'table_name' => Tables::$product_category,
                'condition' => Tables::$product_category . '.cat_id = ' . $this->table_name() . '.cat_id',
                'field' => 'cat_permalink as cat_permalink'
            ),
        );
    }

  

   

    public function get_promo($isreseller = null, $product_id = ""){
        $reseller = ($isreseller == null)?"":" and ".(($isreseller)?"promo.promo_forreseller":"promo.promo_forcustomer")." = 1";
        $product_id = ((!empty($product_id))?" and product.product_id = '$product_id'":"");
        $query = "SELECT promo.*, product.product_id from ".Tables::$promo." as promo INNER JOIN ".Tables::$promo_product." as product on promo.promo_id = product.promo_id $product_id
                    WHERE ('".date('Y-m-d')."' between promo.promo_start and promo.promo_end || promo.promo_start = '".date('Y-m-d')."') AND promo.promo_status = '1' and promo_type = '1' $reseller ";
        $result = $this->ci->db->query($query)->result();
        return $result;
    }

    public function get_free_promo($product_id,$qty=""){
        $minqty = ($qty)?" and pr.promo_min_qty <=  $qty":"";
        $query = "SELECT p.product_name, prod.product_id, p.product_stock, p.product_weight, pr.*, ph.prodphoto_path as product_photo FROM ".Tables::$promo." as pr INNER JOIN ".Tables::$promo_product." as prod on pr.promo_id = prod.promo_id 
                    INNER JOIN ".Tables::$product." p on pr.promo_product_id = p.product_id
                    LEFT JOIN product_photo ph on ph.product_id = p.product_id and ph.prodphoto_isprimary = 1
                              where prod.product_id = '$product_id' and pr.promo_status = 1 and 
                              '".date('Y-m-d')."' between pr.promo_start and pr.promo_end and pr.promo_type = 1 and pr.promo_discounttype = 3 $minqty";
        $result = $this->ci->db->query($query)->row();
        return $result;
    }


    public function get_price($product_id, $varian = "", $reseller_id = 0){
        // $whereproduct = ($ispermalink)?"p.product_permalink = '$product_id'":"p.product_id = '$product_id'";
        $wherevarian = (!empty($varian))?"and pa.attr_varian = '$varian'":"";
        if(!empty($reseller_id)){
          $join = "LEFT JOIN member m on m.member_id = '$reseller_id'";
          $fieldreseller = "m.member_reseller_discount";
        }
        if(!empty($varian)){

        }
        $query = "SELECT * ,IF(reseller_discount > discount_customer, reseller_discount, discount_customer) as discount, product_price - (product_price * IF(reseller_discount > discount_customer, reseller_discount, discount_customer)/100) as price_after_discount   FROM (
                    SELECT p.product_id, p.cat_id, p.product_name, p.product_description, p.product_material, p.product_metaname, p.product_metadescription, 
                    p.product_isbestseller, p.product_seen, p.product_sold, p.product_weight, p.product_minorder, p.product_status, p.product_ispreorder,
                    p.product_ishotsale, p.product_isnewarrival, p.product_ispopular, p.product_rating, 
                    -- IF(IFNULL(p.product_price,0) =0,pa.attr_price,p.product_price) as product_price,
                    IF(pa.attr_varian IS NULL or pa.attr_varian = '', p.product_price, pa.attr_price) as product_price,
                    IFNULL(IF(pr.promo_discount > 0, pr.promo_discount,IF(IFNULL(p.product_discount,0) = 0, pa.attr_discount, p.product_discount)),0) as discount_customer,  ".((@$fieldreseller)?:0)." as reseller_discount, 
                    IF(pa.attr_stock > 0,pa.attr_stock,p.product_stock) as product_stock,
                    ph.prodphoto_path as product_photo,
                    (pr.promo_id is not null) as today_deal
                    FROM product p
                    $wherevarian 
                    LEFT JOIN product_photo ph on ph.product_id = p.product_id and ph.prodphoto_isprimary = 1
                    ".@$join."
                    
                    having price_after_discount = min(price_after_discount)";
        $result = $this->ci->db->query($query)->row();
        if(!empty($result)){
          $free = $this->get_free_promo($result->product_id);
          $result->free_product = (!empty($free))?$free:null;
        }
        return $result;
    }

    public function less_than_5(){
        $sql = "select * from product where product_stock > 0 and product_stock <= 5 and product_status = 1";
        $result = $this->ci->db->query($sql)->result();
        return $result;
    }

    public function outof_stock(){
        $sql = "select * from product where product_stock = 0 and product_status = 1";
        $result = $this->ci->db->query($sql)->result();
        return $result;
    }

    public function today_deal($reseller_id = ""){
        $date = date('Y-m-d');
        $query = "SELECT p.*, pp.product_id from promo p where promo_type = 1 and promo_discounttype = 5 and promo_start = '$date' and promo_status = 1";
        $result = $this->ci->db->query($query)->result();
        $today_deal = array();
        foreach ($result as $row) {
            $p = $this->get_price($row->product_id,"",$reseller_id);
            array_push($today_deal, $p);
        }   
        return $today_deal;
    }

}

?>