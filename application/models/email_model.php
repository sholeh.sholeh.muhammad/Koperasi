<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Email_model extends CI_Model {
	function __construct()
    {
          parent::__construct();
    }
        public function Send_Mail($id, $email, $id_template) {
	/*
        $this->form_validation->set_rules('user_email', 'User Email', 'trim|required|xss_clean');
        $this->form_validation->set_rules('user_password', 'User Password', 'trim|required|xss_clean');
        $this->form_validation->set_rules('to_email', 'To', 'trim|required|xss_clean');
        $this->form_validation->set_rules('subject', 'Subject', 'trim|required|xss_clean');
        $this->form_validation->set_rules('message', 'Message', 'trim|required|xss_clean');
	*/      
		// if ($this->form_validation->run() == FALSE) {
            // $this->load->view('view_form');
        // } else {
		
            if($email=='' || $email==null){
				$mail = $this->db->get_where('sbi_user', array('id' => $id))->row();
				$email = $mail->email;
			}
			$q = $this->db->get_where('notif_template', array('id' => $id_template))->row();
				
            $subject = $q->title;
            $message = $q->detail;
            
            // Configure email library
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'mail.smtp2go.com';
            $config['smtp_port'] = 2525;
            $config['smtp_user'] = 'info@edumatic.info';
            $config['smtp_pass'] = 'T4E451110';
			$config['mailtype']  = 'html';
			$config['charset']  = 'iso-8859-1';
            // Load email library and passing configured values to email library 
            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            
            // Sender email address
            $this->email->from('info@edumatic.info', 'Edumatic');
            // Receiver email address
            $this->email->to($email);
            // Subject of email
            $this->email->subject($subject);
            // Message in email
            $this->email->message($message);

            if ($this->email->send()) {
                // $data['message_display'] = 'Email Successfully Send !';
            } else {
                // $data['message_display'] = '<p class="error_msg">Invalid Gmail Account or Password !</p>';
            }
            // $this->load->view('view_form', $data);
        // }
    }

		public function Send_Mail_resPassword($email) {

            $subject = "Lupa Kata Sandi";
			$root = "localhost:81/scm/login/resetPassword?mail=".base64_encode($email);
			// $root = str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
            $message = '<h1>Lupa kata Sandi</h1>
			Klik link di bawah untuk melakukan reset password
			<br>
			<a style="font-size:18px" href="'.$root.'">reset</a>
			';
            
            // Configure email library
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'mail.smtp2go.com';
            $config['smtp_port'] = 2525;
            $config['smtp_user'] = 'sholeh.sholeh.muhammad@gmail.com';
            $config['smtp_pass'] = 'dzZvZzJyYzkzN28w';
			$config['mailtype']  = 'html';
			$config['charset']  = 'iso-8859-1';

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            
            // Sender email address
            $this->email->from('ginagiovani19@yahoo.co.id', 'Asih Hati Furniture');
            // Receiver email address
            $this->email->to($email);
            // Subject of email
            $this->email->subject($subject);
            // Message in email
            $this->email->message($message);

            if ($this->email->send()) {
                $data['message_display'] = 'Buka Tautan pada email anda untuk melanjutkan !';
            } else {
                // $data['message_display'] = '<p class="error_msg">Invalid  Account or Password !</p>';
            }
            // $this->load->view('view_form', $data);
        // }
    }
	public function Send_Mail_newTable($id,$table) {
            $mail = $this->db->get_where('sbi_user', array('id' => $id))->row();
			$email = $mail->email;
			$name =  $mail->full_name;
            $subject = "New Table Created";
//			$root = "http://198.50.245.86/BAM/index.php/login/resetPassword?mail=".base64_encode($id);
			// $root = str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
            $message = '<h1>New Table</h1>
			New Table <b>'.$table.'</b> has been created by '.$name.' 
			<br>
			';

            // Configure email library
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'mail.smtp2go.com';
            $config['smtp_port'] = 2525;
            $config['smtp_user'] = 'info@edumatic.info';

            $config['smtp_pass'] = 'T4E451110';
			$config['mailtype']  = 'html';
			$config['charset']  = 'iso-8859-1';

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            
            // Sender email address
            $this->email->from($email, $name);
            // Receiver email address
            $this->email->to('info@edumatic.info');
            // Subject of email
            $this->email->subject($subject);
            // Message in email
            $this->email->message($message);

            if ($this->email->send()) {
                // $data['message_display'] = 'Email Successfully Send !';
            } else {
                // $data['message_display'] = '<p class="error_msg">Invalid Gmail Account or Password !</p>';
            }
            // $this->load->view('view_form', $data);
        // }
    }

	public function Send_Mail_updateTable($id,$table) {
            $mail = $this->db->get_where('sbi_user', array('id' => $id))->row();
			$email = $mail->email;
			$name =  $mail->full_name;
            $subject = "Table Updated";
//			$root = "http://198.50.245.86/BAM/index.php/login/resetPassword?mail=".base64_encode($id);
			// $root = str_replace(basename($_SERVER['SCRIPT_NAME']),"",$_SERVER['SCRIPT_NAME']);
            $message = '<h1>Update Table</h1>
			Table <b>'.$table.'</b> has been Updated by '.$name.' 
			<br>
			';

            // Configure email library
            $config['protocol'] = 'smtp';
            $config['smtp_host'] = 'mail.smtp2go.com';
            $config['smtp_port'] = 2525;
            $config['smtp_user'] = 'info@edumatic.info';
            $config['smtp_pass'] = 'T4E451110';
			$config['mailtype']  = 'html';
			$config['charset']  = 'iso-8859-1';

            $this->load->library('email', $config);
            $this->email->set_newline("\r\n");
            
            // Sender email address
            $this->email->from($email, $name);
            // Receiver email address
            $this->email->to('info@edumatic.info');
            // Subject of email
            $this->email->subject($subject);
            // Message in email
            $this->email->message($message);

            if ($this->email->send()) {
                // $data['message_display'] = 'Email Successfully Send !';
            } else {
                // $data['message_display'] = '<p class="error_msg">Invalid Gmail Account or Password !</p>';
            }
            // $this->load->view('view_form', $data);
        // }
    }

}