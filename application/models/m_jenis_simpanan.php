<?php
class M_jenis_simpanan extends Generic_dao {

    public function table_name() {
        return Tables::$jenis_simpanan;
    }

    public function field_map() {
		return array(
			'id_jenis'         => 'id_jenis',
			'nama_simpanan'    => 'nama_simpanan',
			'besar_simpanan'   => 'besar_simpanan',
			'type'             => 'type',
			'keterangan'       => 'keterangan'
		);
    }

    public function __construct() {
        parent::__construct();
    }

    public function get_category($parent = null){
//    	$where = array('parent_cat_id'=>$parent, 'cat_status'=>1);
    	return $this->ci->db->get($this->table_name())->result();
    }

    public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$jenis_simpanan ." as parent ",
                'condition' => 'parent.id_jenis = '.$this->table_name().'.id_jenis',
                'field' => 'parent.nama_simpanan as parent_cat_name',
                'direction' => 'left'
            )
        );
    }

}

?>