<?php
class M_product_category extends Generic_dao {

    public function table_name() {
        return Tables::$product_category;
    }

    public function field_map() {
		return array(
			'cat_id' => 'cat_id',
			'parent_cat_id' => 'parent_cat_id',
			'cat_name' => 'cat_name',
			'cat_image' => 'cat_image',
			'cat_status' => 'cat_status',
			'cat_permalink'=> 'cat_permalink',
			'created_by' => 'created_by',
			'created_on' => 'created_on',
			'updated_by' => 'updated_by',
			'updated_on' => 'updated_on'
		);
    }

    public function __construct() {
        parent::__construct();
    }

    public function get_category($parent = null){
    	$where = array('parent_cat_id'=>$parent, 'cat_status'=>1);
    	return $this->ci->db->get_where($this->table_name(),$where)->result();
    }

    public function joined_table() {
        return array(
            array(
                'table_name' => Tables::$product_category ." as parent ",
                'condition' => 'parent.cat_id = '.$this->table_name().'.parent_cat_id',
                'field' => 'parent.cat_name as parent_cat_name',
                'direction' => 'left'
            )
        );
    }

}

?>