<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Sholeh
    <small>Insert</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">Sholeh</a></li>
    <li class="active">Insert</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><strong>Error! </strong>'.$message.'</div></div>': '';
    ?>
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Sholeh</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <form role="form" method="POST" enctype="multipart/form-data" class="form-horizontal">
            
			<div class="form-group <?php echo (form_error('last_name') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Last name</label>
          <div class="col-md-10">
            <input class="form-control " name="last_name" value="<?php echo set_value('last_name', $sholeh->last_name); ?>" placeholder="Role Name"  required  maxlength=50>
          </div>
				<?php echo form_error('last_name'); ?>
			</div>
			<div class="form-group <?php echo (form_error('dept_id') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-2">Dept ID</label>
			<div class="col-md-10">
				<input class="form-control " name="dept_id" value="<?php echo set_value('dept_id', $sholeh->dept_id); ?>" placeholder="Role Name"  required  maxlength=50>
			</div>
				<?php echo form_error('dept_id'); ?>
			</div>
			<div class="form-group <?php echo (form_error('salary') != "") ? "has-error" : "" ?>">
				<label class="control-label col-md-2">Salary</label>
			<div class="col-md-10">
				<input class="form-control " name="salary" value="<?php echo set_value('salary', $sholeh->salary); ?>" placeholder="Role Name"  required  maxlength=50>
			</div>
				<?php echo form_error('salary'); ?>
			</div>
			<div class="form-group">
      </div>
            <div class="box-footer">
               <a href="<?php echo $current_context; ?>" class="btn btn-default">Cancel</a>
               <button type="submit" class="btn btn-primary pull-right">Save</button>
            </div><!-- /.box-footer -->
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->