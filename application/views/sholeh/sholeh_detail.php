<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Sholeh
    <small>Detail</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">Sholeh</a></li>
    <li class="active">Detail</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Sholeh</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
            
			<div class="form-group">
					<label>Nama:</label>
					<p><?php echo $sholeh->last_name; ?></p>
			</div>
			<div class="form-group">
					<label>Status:</label>
					<p><?php echo $sholeh->dept_id; ?></p>
			</div>
			<div class="form-group">
					<label>Dapat Login:</label>
					<p><?php echo $sholeh->salary; ?></p>
			</div>
            <div class="box-footer">
               <a href="<?php echo $current_context; ?>" class="btn btn-default">Back</a>
               <a href="<?php echo $current_context .'edit/'.$sholeh->employee_id ; ?>" class="btn btn-primary pull-right">Edit</a>
            </div><!-- /.box-footer -->
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->