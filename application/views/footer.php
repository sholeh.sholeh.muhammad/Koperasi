    <footer class="main-footer">
        <div class="pull-right hidden-xs">
			MAS
        </div>
        <strong>Copyright &copy; 2016 <a href=""></a>.</strong> All rights reserved.
    </footer>
	<audio id="notif_audio"><source src="<?php echo base_url('sounds/notify.ogg');?>" type="audio/ogg"><source src="<?php echo base_url('sounds/notify.mp3');?>" type="audio/mpeg"><source src="<?php echo base_url('sounds/notify.wav');?>" type="audio/wav"></audio>	
	 <script src="<?=base_url('assets/plugins/libs/moment.min.js')?>"></script>
	 <script src="<?=base_url('assets/plugins/combodate/combodate.js')?>"></script>
	 <script src="<?=base_url('assets/js/highcharts.js')?>"></script>
	<script src="<?=base_url('assets/js/highcharts-3d.js')?>"></script>	 
	 <script>
	 $(function(){
		$(".combodate").each(function(){ 
		$(this).combodate();
			$(this).next('.combodate').find('select').addClass('form-control');
		});

	});
		$(document).ready(function(){
			$.ajax({
				type: "POST",
				url: "<?php echo base_url('notif/new_count_notif');?>",
				//data: dataString,
				dataType: "json",
				cache : false,
				success: function(data){
										// alert(data.count_notif);
					$("#new_count_notif").html(data.count_notif);
				} ,error: function(xhr, status, error) {
				  alert(error);
				},

			});
		});		
	// var port = 8000;
	
	var socket = io.connect( 'http://'+window.location.hostname+':'+3000 );
			// socket.on( 'new_count_notif_pemesanan', function( data ) {
				// alert("notif");
				// $('#notif_audio')[0].play();
			// });
	  // socket.on( 'new_count_notif', function( data ) {
	  
		  // $( "#new_count_message" ).html( data.new_count_message );
		  // $('#notif_audio')[0].play();

	  // });

	  // socket.on( 'update_count_message', function( data ) {

		  // $( "#new_count_message" ).html( data.update_count_message );
		
	  // });
	var role = "<?=$this->session->userdata('level');?>";
	<?php
		$un = $this->session->userdata('username');
		$gj	= $this->db->get_where('petugas', array('username'=>$un))->row();
	?>
	var jbt = "<?=$gj->jabatan?>";
	if(role=='petugas'){
		if(jbt=="produksi" || jbt=="admin"){
			socket.on( 'new_count_notif_pemesanan', function( data ) {
				// $( "#new_count_notif" ).html( data.new_count_notif_pemesanan );
				// alert(data.new_count_notif_pemesanan);
				if(data.new_count_notif_pemesanan=="proses" && jbt=="produksi"){
					$('#notif_audio')[0].play();
					$.ajax({
						type: "POST",
						url: "<?php echo base_url('notif/new_count_notif');?>",
						//data: dataString,
						dataType: "json",
						cache : false,
						success: function(data){
												// alert(data.count_notif);
							$("#new_count_notif").html(data.count_notif);
						} ,error: function(xhr, status, error) {
						  alert(error);
						},

					});		
				}
				
				if(data.new_count_notif_pemesanan!="proses" && jbt=="admin"){
					$('#notif_audio')[0].play();
					$.ajax({
						type: "POST",
						url: "<?php echo base_url('notif/new_count_notif');?>",
						//data: dataString,
						dataType: "json",
						cache : false,
						success: function(data){
												// alert(data.count_notif);
							$("#new_count_notif").html(data.count_notif);
						} ,error: function(xhr, status, error) {
						  alert(error);
						},

					});		
				}
				
				
			});
		}
		
		if(jbt=="pemesanan" || jbt=="admin"){
			socket.on( 'new_count_notif_pemesanan', function( data ) {
				// $( "#new_count_notif" ).html( data.new_count_notif_pemesanan );
				// alert(data.new_count_notif_pemesanan);
				$('#notif_audio')[0].play();
				if(data.new_count_notif_pemesanan=="tunggu"){
					$.ajax({
						type: "POST",
						url: "<?php echo base_url('notif/new_count_notif');?>",
						//data: dataString,
						dataType: "json",
						cache : false,
						success: function(data){
												// alert(data.count_notif);
							$("#new_count_notif").html(data.count_notif);
						} ,error: function(xhr, status, error) {
						  alert(error);
						},

					});		
				}
			});
		}
		
		if(jbt=="gudang" || jbt=="admin"){
			socket.on( 'new_count_notif_pengadaan', function( data ) {
				$('#notif_audio')[0].play();
				alert(data.new_count_notif_pengadaan);
			});
		}
		
		
	}
	// socket.on( 'task_notif', function( data ) {
			// $("#task").prepend('<li style="background:#eee !important;">\
									// <a href="#">\
										// <h4>\
											// '+data.title+' - '+data.from+'\
										// </h4>\
										// <p>'+data.detail+'</p>\
									// </a>\
								// </li>');
		// });

		// socket.on( 'konfirmasi_donasi', function( data ) {
			// $("#task").prepend('<li>\
									// <a href="#">\
										// <h4>\
											// '+data.title+' - '+data.from+'\
										// </h4>\
										// <p>'+data.detail+'</p>\
									// </a>\
								// </li>');
		// });
		
		// socket.on( 'new_count_task', function( data ) {
			// alert(data.new_count_task);
			// $( "#new_count_task" ).html( data.new_count_task );
			// $('#notif_audio')[0].play();
			// update_task();
		// });

	</script>
	
	 