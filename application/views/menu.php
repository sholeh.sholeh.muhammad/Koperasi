    <section>
        <!-- Left Sidebar -->
        <aside id="leftsidebar" class="sidebar">
            <!-- User Info -->
            <div class="user-info">
                <div class="image">
                    <img src="images/user.png" width="48" height="48" alt="User" />
                </div>

                <div class="info-container">
                    <div class="name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><?=$this->session->userdata('nama')?></div>
                    <div class="email"></div>
                    <div class="btn-group user-helper-dropdown">
                        <i class="material-icons" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">keyboard_arrow_down</i>
                        <ul class="dropdown-menu pull-right">
                            <li><a href="javascript:void(0);"><i class="material-icons">person</i>Profile</a></li>
                            <li role="seperator" class="divider"></li>
                            <li><a href="<?php echo site_url('login/logout')?>"><i class="material-icons">input</i>Sign Out</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- #User Info -->
            <!-- Menu -->
            <div class="menu">

                <ul class="list">
                    <li class="header">MAIN NAVIGATION</li>
			<?php /*
				$level		= $this->session->userdata('level');
				$username	= $this->session->userdata('username');
				$jabatan = $this->db->query("select jabatan from petugas where username='".$username."' ")->row();

						$menu = $this->db->get_where('menu', array('is_parent' => 0,'is_active'=>1));
                        foreach ($menu->result() as $m) {
                            // chek ada sub menu
                            $submenu = $this->db->get_where('menu',array('is_parent'=>$m->id,'is_active'=>1));
                            if($submenu->num_rows()>0){

								if(in_array($jabatan->jabatan,explode(',',$m->user_list))){
									foreach ($submenu->result() as $s){
										if(in_array($jabatan->jabatan,explode(',',$s->user_list))){
											if ( $this->uri->uri_string() == $s->link ){
												$c1='active';
												break;
											}else{
												$c1=$this->uri->uri_string();
											}
										}else{
										}
									}

									echo "<li class=' ".$c1."'>
									                        <a href='javascript:void(0);' class='menu-toggle'>
                            <i class='material-icons'>widgets</i>
                            <span>".strtoupper($m->name)."</span>
                        </a>
											<ul class='ml-menu'>";
								}

                                foreach ($submenu->result() as $s){
									if(in_array($jabatan->jabatan,explode(',',$s->user_list))){
										if ( $this->uri->uri_string() == $s->link ){
											$c2='active';
										}else{
											$c2='';
										}
										echo "<li class=".$c2.">" . anchor($s->link, "<i class='fa ".$s->icon."'></i> <span>" . strtoupper($s->name)) . "</span></li>";
									}else{
										// echo "<li>" . anchor($s->link, "<i class='$s->icon'></i> <span>" . strtoupper($s->name)) . "</span></li>";
									}
                                }
                                   echo"</ul>
                                    </li>";
                            }else{
                                echo "<li>" . anchor($m->link, "<i class='fa ".$m->icon."'></i> <span>" . strtoupper($m->name)) . "</span></li>";
                            }

                        }
			*/?>


                </ul>
            </div>
            <!-- #Menu -->
            <!-- Footer -->
            <div class="legal">
                <!--div class="copyright">
                    &copy; 2016 <a href="javascript:void(0);">AdminBSB - Material Design</a>.
                </div>
                <div class="version">
                    <b>Version: </b> 1.0.3
                </div-->
            </div>
            <!-- #Footer -->
        </aside>
        <!-- #END# Left Sidebar -->
        <!-- Right Sidebar -->
    </section>
