<div class="row">
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">Jenis Simpanan</a></li>
    <li class="active"><?php echo (!$edit)?'Insert':'Edit' ?></li>
  </ol>

<!-- Main content -->
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><strong>Error! </strong>'.$message.'</div></div>': '';
    ?>
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box box-warning">
        <div class="box-header with-border">
        </div><!-- /.box-header -->
        <div class="box-body">
          <form role="form" method="POST" enctype="multipart/form-data" class="form-horizontal">
            
			<div class="form-group <?php echo (form_error('parent_cat_id') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Nama Jenis</label>
					<div class="col-md-10">
					</div>
					<!--<input class="form-control mask_number" name="parent_menu_id" value="<?php echo set_value('parent_menu_id', $menu->parent_menu_id); ?>" placeholder="Men Menu Id"  maxlength=10>-->
				<?php echo form_error('parent_menu_id'); ?>
			</div>
			<div class="form-group <?php echo (form_error('besar_simpanan') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Besar Simpanan</label><div class="col-md-10"><input class="form-control " name="besar_simpanan" value="<?php echo set_value('besar_simpanan', ''); ?>" placeholder="Besar Simpanan"  required  maxlength=50></div>
				<?php echo form_error('besar_simpanan'); ?>
			</div>
			<div class="form-group <?php echo (form_error('type') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Tipe</label><div class="col-md-10"><input class="form-control " name="type" value="<?php echo set_value('type', ''); ?>" placeholder="Tipe"  required  maxlength=100></div>
				<?php echo form_error('type'); ?>
			</div>
			<div class="form-group <?php echo (form_error('keterangan') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Keterangan</label><div class="col-md-10">
					<input class="form-control " name="keterangan" value="<?php echo set_value('keterangan', ''); ?>" placeholder="keterangan"  required  maxlength=100></div>
				<?php echo form_error('keterangan'); ?>
			</div>
            <div class="box-footer">
               <a href="<?php echo $current_context; ?>" class="btn btn-default">Batal</a>
               <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            </div><!-- /.box-footer -->
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
