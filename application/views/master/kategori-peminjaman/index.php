  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>koperasi"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Jenis Simpanan</li>
  </ol>

<!-- Main content -->
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-check"></i> Great!</h4>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-exclamation-triangle"></i> Uh-Oh!</h4>'.$message.'</div></div>': '';
    ?>
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Collections <span class="badge">

          <?php echo $total_rows; ?> Data</span>
          </h3>
          <div class="box-tools pull-right">
            <i class="fa fa-refresh fa-spin fa-lg fa-fw loading-icon hide" aria-hidden="true"></i>
            <span class="sr-only">Refreshing...</span>
            <a href="<?php echo $current_context . 'add/'.(!empty($parent1)?$parent1.'/':'').(!empty($parent2)?$parent2:''); ?>" class="btn btn-xs bg-light-blue">
                <i class="fa fa-plus"></i>&nbsp; Add
            </a>
            <a href="javascript:;" class="btn btn-xs bg-orange refresh-page"><i class="fa fa-refresh"></i> Refresh</a>
            <button class="btn btn-box-tool btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-2">
              <dl class="text-right">
                <dt><h5>Filter Data</h5></dt>
                <dd>type/ select to find or filter data</dd>
              </dl>
            </div>
          <div class="col-md-10">
            <form method="post" action="<?php echo $current_context . 'search'; ?>">
                <?php $key = (object) $this->session->userdata('filter_product_category'); ?>
                <div class="row">
                 <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Nama Simpanan</label>
                      <input class="form-control " name="cat_name" value="<?php echo $key->nama_simpanan; ?>" placeholder="Name">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="clearfix pull-left">
                        <input type="hidden" id="search" name="search" value="true">
                        <button type="submit" class="btn btn-sm btn-primary">Search</button>
                        <button type="button" class="btn btn-sm btn-default" onclick="location.href='<?php echo $current_context; ?>'">Clear</button>
                    </div>
                  </div>
                </div> 
              </form>
          </div>
         </div>
        </div><!-- /.box-body -->
        <div class="box-header">
         <!--  <h3 class="box-title"><?php echo (!empty($category))?"Category : ".$category->cat_name:"Data Table"; ?> (<?php echo $total_rows; ?> Data)</h3>
            <div class="box-tools pull-right">
              <a href="<?php echo $current_context . 'add/'.(($subcat_id)?:(($cat_id)?:'')); ?>" class="btn btn-sm bg-light-blue">
                  <i class="fa fa-plus"></i>&nbsp; Add
              </a>
              <button id="deleteall" class="btn bg-red btn-sm" data-href="<?php echo $current_context . 'delete_multiple'; ?>" data-toggle="modal" data-target="#deleteAll"><i class="fa fa-trash-o"></i> Delete All</button>
            </div> -->
        </div><!-- /.box-header -->
       <div class="box-body">
        <div class="row">
          <div class="col-md-2">
              <dl class="text-right">
                <dt><h5>Data</h5></dt>
                <dd>data table, all data restore here</dd>
              </dl>
              <div class="clearfix text-right">
                <p><a href="<?php echo $current_context . 'add/'.(!empty($parent1)?$parent1.'/':'').(!empty($
                parent2)?$parent2:''); ?>" class="btn btn-sm bg-light-blue btn-block">
                    <i class="fa fa-plus"></i>Add Jenis
                </a></p>
                <p>
                  <!-- <button id="deleteall" class="btn bg-red btn-block btn-sm" data-href="<?php echo $current_context . 'delete_multiple'; ?>" data-toggle="modal" data-target="#deleteAll"><i class="fa fa-trash-o"></i> Delete Selected</button> -->
                  <span class="slctd-dta hide"></span>
                </p>
              </div>
            </div>

      <div class="col-md-10">
       <div class="table-responsive">
          <table class="table table-bordered table-hover table-striped" id="table_data">
          <thead>
            
            <tr>
              <th class="table-checkbox">
              No.
              </th>
            <th>Jenis</th>
            <th>Besar Simpanan</th>
            <th>Type</th>
            <th>Keterangan</th>
            <th>Action</th>
            </tr>
          </thead>       
            <?php
            if(!empty($product_category)){ 
            $i = $offset + 1;
            foreach ($product_category as $row) {
                ?>
                <tr>
                    <td> 
                      <div class="checkbox"><label>
                        <?php echo $i; ?></label>
                        </div>
                      </td>
                      <td><?php echo $row->nama_simpanan; ?></td>
                      <td><?php echo $row->besar_simpanan; ?></td>
                      <td><?php echo $row->type; ?></td>
                      <td><?php echo $row->keterangan; ?></td>
                      </td>
                      <td class="td-btn">
                       <!--  <?php if(empty($subcat_id)) { ?>
                        <a href="<?php echo $current_context . 'index/1/'.((!empty($cat_id))?"$cat_id/":""). $row->cat_id ?>" class="badge bg-green"><i class="fa fa-ellipsis-h fa-fw"></i>Sub Collection</a>
                        <?php } ?> -->

                        <a href="<?php echo $current_context . 'edit/'. $row->id_jenis ?>" class="badge bg-green"><i class="fa fa-edit fa-fw"></i>View</a>
                        <a href="#" data-href="<?php echo $current_context . "delete/$row->id_jenis"?>" data-toggle="modal" data-target="#deleteModal"  class="badge bg-red"><i class="fa fa-trash-o fa-fw"></i>Delete</a>
                      </td>
                </tr>
            <?php 
            $i++; 
             }
            } else if($this->uri->segment(3) == 'search') { 
            ?>
                    <tr>
                        <td colspan="6" class="empty-table">
                          <br>
                          <p class="text-center"><i class="fa fa-th-list fa-3x"></i></p>
                          <p class="text-center"><i>We couldn't find the data you're looking for.</p></i>
                          <br>
                        </td>
                      </tr>
            <?php 
            } else { 
              ?> 
                    <tr>
                        <td colspan="6" class="empty-table">
                          <br>
                          <p class="text-center"><i class="fa fa-th-list fa-3x"></i></p>
                          <p class="text-center"><i><a href="<?php echo $current_context . 'add/'.(!empty($parent1)?$parent1.'/':'').(!empty($parent2)?$parent2:''); ?>">Add</a> your Collections now.</p></i>
                          <br>
                        </td>
                      </tr>

            <?php }?>
            <tfoot>
              <th></th>
              <th>Image</th>
              <th>Name</th>
              <th>Status</th>
              <th>Action</th>
            </tfoot>
          </table>
          
          <?php if(!empty($back_url)) { ?>
          <div class="box-header">
              <a href="<?php echo $back_url; ?>" class="btn btn-sm bg-light-blue">
                  <i class="fa fa-arrow-left"></i>&nbsp; Back
              </a>
          </div>
          <?php } ?>
         </div>

         </div>
        </div>
       </div><!-- /.box-body -->
       <div class="box-footer clearfix">
            <div class="col-sm-6">
              
            </div>
            <div class="col-sm-6">
              <div class="pull-right">
              <?php echo $pagination; ?>
              </div>
            </div>
        </div>
      </div><!-- /.box -->
    </div>
  </div>  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>koperasi"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Jenis Simpanan</li>
  </ol>

<!-- Main content -->
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-check"></i> Great!</h4>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-exclamation-triangle"></i> Uh-Oh!</h4>'.$message.'</div></div>': '';
    ?>
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Collections <span class="badge">

          <?php echo $total_rows; ?> Data</span>
          </h3>
          <div class="box-tools pull-right">
            <i class="fa fa-refresh fa-spin fa-lg fa-fw loading-icon hide" aria-hidden="true"></i>
            <span class="sr-only">Refreshing...</span>
            <a href="<?php echo $current_context . 'add/'.(!empty($parent1)?$parent1.'/':'').(!empty($parent2)?$parent2:''); ?>" class="btn btn-xs bg-light-blue">
                <i class="fa fa-plus"></i>&nbsp; Add
            </a>
            <a href="javascript:;" class="btn btn-xs bg-orange refresh-page"><i class="fa fa-refresh"></i> Refresh</a>
            <button class="btn btn-box-tool btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-2">
              <dl class="text-right">
                <dt><h5>Filter Data</h5></dt>
                <dd>type/ select to find or filter data</dd>
              </dl>
            </div>
          <div class="col-md-10">
            <form method="post" action="<?php echo $current_context . 'search'; ?>">
                <?php $key = (object) $this->session->userdata('filter_product_category'); ?>
                <div class="row">
                 <div class="col-md-6">
                    <div class="form-group">
                      <label class="control-label">Nama Simpanan</label>
                      <input class="form-control " name="cat_name" value="<?php echo $key->nama_simpanan; ?>" placeholder="Name">
                    </div>
                  </div>
                  <div class="col-md-12">
                    <div class="clearfix pull-left">
                        <input type="hidden" id="search" name="search" value="true">
                        <button type="submit" class="btn btn-sm btn-primary">Search</button>
                        <button type="button" class="btn btn-sm btn-default" onclick="location.href='<?php echo $current_context; ?>'">Clear</button>
                    </div>
                  </div>
                </div> 
              </form>
          </div>
         </div>
        </div><!-- /.box-body -->
        <div class="box-header">
         <!--  <h3 class="box-title"><?php echo (!empty($category))?"Category : ".$category->cat_name:"Data Table"; ?> (<?php echo $total_rows; ?> Data)</h3>
            <div class="box-tools pull-right">
              <a href="<?php echo $current_context . 'add/'.(($subcat_id)?:(($cat_id)?:'')); ?>" class="btn btn-sm bg-light-blue">
                  <i class="fa fa-plus"></i>&nbsp; Add
              </a>
              <button id="deleteall" class="btn bg-red btn-sm" data-href="<?php echo $current_context . 'delete_multiple'; ?>" data-toggle="modal" data-target="#deleteAll"><i class="fa fa-trash-o"></i> Delete All</button>
            </div> -->
        </div><!-- /.box-header -->
       <div class="box-body">
        <div class="row">
          <div class="col-md-2">
              <dl class="text-right">
                <dt><h5>Data</h5></dt>
                <dd>data table, all data restore here</dd>
              </dl>
              <div class="clearfix text-right">
                <p><a href="<?php echo $current_context . 'add/'.(!empty($parent1)?$parent1.'/':'').(!empty($parent2)?$parent2:''); ?>" class="btn btn-sm bg-light-blue btn-block">
                    <i class="fa fa-plus"></i>Add Jenis
                </a></p>
                <p>
                  <!-- <button id="deleteall" class="btn bg-red btn-block btn-sm" data-href="<?php echo $current_context . 'delete_multiple'; ?>" data-toggle="modal" data-target="#deleteAll"><i class="fa fa-trash-o"></i> Delete Selected</button> -->
                  <span class="slctd-dta hide"></span>
                </p>
              </div>
            </div>

      <div class="col-md-10">
       <div class="table-responsive">
          <table class="table table-bordered table-hover table-striped" id="table_data">
          <thead>
            
            <tr>
              <th class="table-checkbox">
              No.
              </th>
            <th>Jenis</th>
            <th>Besar Simpanan</th>
            <th>Type</th>
            <th>Keterangan</th>
            <th>Action</th>
            </tr>
          </thead>       
            <?php
            if(!empty($product_category)){ 
            $i = $offset + 1;
            foreach ($product_category as $row) {
                ?>
                <tr>
                    <td> 
                      <div class="checkbox"><label>
                        <?php echo $i; ?></label>
                        </div>
                      </td>
                      <td><?php echo $row->nama_simpanan; ?></td>
                      <td><?php echo $row->besar_simpanan; ?></td>
                      <td><?php echo $row->type; ?></td>
                      <td><?php echo $row->keterangan; ?></td>
                      </td>
                      <td class="td-btn">
                       <!--  <?php if(empty($subcat_id)) { ?>
                        <a href="<?php echo $current_context . 'index/1/'.((!empty($cat_id))?"$cat_id/":""). $row->cat_id ?>" class="badge bg-green"><i class="fa fa-ellipsis-h fa-fw"></i>Sub Collection</a>
                        <?php } ?> -->

                        <a href="<?php echo $current_context . 'edit/'. $row->id_jenis ?>" class="badge bg-green"><i class="fa fa-edit fa-fw"></i>View</a>
                        <a href="#" data-href="<?php echo $current_context . "delete/$row->id_jenis"?>" data-toggle="modal" data-target="#deleteModal"  class="badge bg-red"><i class="fa fa-trash-o fa-fw"></i>Delete</a>
                      </td>
                </tr>
            <?php 
            $i++; 
             }
            } else if($this->uri->segment(3) == 'search') { 
            ?>
                    <tr>
                        <td colspan="6" class="empty-table">
                          <br>
                          <p class="text-center"><i class="fa fa-th-list fa-3x"></i></p>
                          <p class="text-center"><i>We couldn't find the data you're looking for.</p></i>
                          <br>
                        </td>
                      </tr>
            <?php 
            } else { 
              ?> 
                    <tr>
                        <td colspan="6" class="empty-table">
                          <br>
                          <p class="text-center"><i class="fa fa-th-list fa-3x"></i></p>
                          <p class="text-center"><i><a href="<?php echo $current_context . 'add/'.(!empty($parent1)?$parent1.'/':'').(!empty($parent2)?$parent2:''); ?>">Add</a> your Collections now.</p></i>
                          <br>
                        </td>
                      </tr>

            <?php }?>
            <tfoot>
              <th></th>
              <th>Image</th>
              <th>Name</th>
              <th>Status</th>
              <th>Action</th>
            </tfoot>
          </table>
          
          <?php if(!empty($back_url)) { ?>
          <div class="box-header">
              <a href="<?php echo $back_url; ?>" class="btn btn-sm bg-light-blue">
                  <i class="fa fa-arrow-left"></i>&nbsp; Back
              </a>
          </div>
          <?php } ?>
         </div>

         </div>
        </div>
       </div><!-- /.box-body -->
       <div class="box-footer clearfix">
            <div class="col-sm-6">
              
            </div>
            <div class="col-sm-6">
              <div class="pull-right">
              <?php echo $pagination; ?>
              </div>
            </div>
        </div>
      </div><!-- /.box -->
    </div>
  </div>