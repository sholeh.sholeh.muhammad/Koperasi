  <?php $this->load->view('header'); ?>
	<body class="hold-transition login-page">
    <div class="login-box">
      <div class="login-logo">
        <a href="#"><h1>Asih Hati Furniture</h1></a>
      </div><!-- /.login-logo -->
      <div class="login-box-body">
        <p class="login-box-msg">Masukan Email</p>
		<div id="message"><?php echo $message;?></div>
        <form action="<?php echo site_url('login/forgot_password')?>" class="panel-body wrapper-lg" method="post" >
		<?php if ($message!=null||$message!=''): ?>
          <div class="alert alert-danger">
            <a data-dismiss="alert" href="#"></a><?=$message?>
          </div>
        <?php endif; ?>

          <div class="form-group has-feedback">
            <label class="control-label ">Email</label>
			<input type="text" id="email" class="form-control input-lg" name="email" placeholder="Email" >
            <span class="fa fa-envelope form-control-feedback"></span>			
          </div>
          <button type="submit" class="btn btn-primary">Send</button>
			<br/><br/><a href="<?=site_url()?>">Batal</a><br>
        </form>
      </div>
    </div>
	</body>

