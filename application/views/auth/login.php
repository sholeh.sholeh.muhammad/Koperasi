  <?php $this->load->view('header'); ?>
  <style>
      .Valor {color: #FF0000;}
      .success{font-size:18px; color:#00FF00}
      table{border : 4px Solid #AF1500; padding:10px;}
    </style>
  <body class="login-page">
    <div class="login-box">
        <div class="logo">
            <a href="#"><img src="<?=base_url('assets/images');?>/logo.png" width="150" ></a>
             <a href="javascript:void(0);" style="color:#00913F">Koperasi<b>SP</b></a>
            <small></small>
        </div>
        <div class="card">
            <div class="body">
		<div id="message"></div>
        <form action="<?php echo site_url('login/login_post')?>" method="post">
                    <div class="msg">Silahkan Login</div>
                    <div class=Valor><?=isset($invalid)?$invalid:''?></div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">person</i>
                        </span>
                        <div class="form-line">
                            <input type="text" class="form-control" name="username" placeholder="Username" required autofocus>                            
                        </div>
                        <div class=Valor><?=isset($error_string['username'])?$error_string['username']:''?></div>
                    </div>
                    <div class="input-group">
                        <span class="input-group-addon">
                            <i class="material-icons">lock</i>
                        </span>
                        <div class="form-line">
                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                        </div>
                        <div class=Valor><?=isset($error_string['password'])?$error_string['password']:''?></div>
                    </div>
                    <div class="row">
                        <div class="col-xs-8 p-t-5">
                            <input type="checkbox" name="rememberme" id="rememberme" class="filled-in chk-col-pink">
                            <label for="rememberme">Remember Me</label>
                        </div>
                        <div class="col-xs-4">
                            <button class="btn btn-block bg-green waves-effect" type="submit" name="login">SIGN IN</button>
                        </div>
                    </div>
                    <div class="row m-t-15 m-b--20">
                        <div class="col-xs-6">
                            <a href="sign-up.html">Register Now!</a>
                        </div>
                        <div class="col-xs-6 align-right">
							<a href="<?=site_url("login/forgot_password")?>">Lupa Password ?</a><br>
                        </div>
                    </div>
              </form>
            </div>
        </div>
    </div>


</body>

	