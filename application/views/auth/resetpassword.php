<script type="text/javascript" src="<?php echo base_url('assets/js/jquery.min.js')?>" ></script>
  
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/bootstrap.css')?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/animate.css')?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font-awesome.min.css')?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/font.css')?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/app.css')?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/watable.css')?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/css/datepicker.css')?>" />
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/js/datatables.css')?>" />
<script type="text/javascript" src="<?php echo base_url('assets/codebase/webix.js')?>" ></script>
<link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/codebase/webix.css')?>" />  
<title>BAM</title>
<section id="content" class="m-t-lg wrapper-md animated fadeInUp">    
    <div class="container aside-xxl">
      <a class="navbar-brand block" href="#">BAM (Bussiness Analytic Manager)</a>
      <section class="panel panel-default bg-white m-t-lg">
        <header class="panel-heading text-center">
          <strong>Insert your new password</strong>
        </header>
			<form action="#" class="panel-body wrapper-lg" method="post" id="form" >
			  <div class="form-group">
				<input type="password" class="form-control" placeholder="new password" name="password">
				<span class="help-block"></span>
				<span class="form-control-feedback"></span>
			  </div>
			  <div class="form-group">
				<input type="password" class="form-control" placeholder="Retype new password" name="r_password">
				<span class="help-block"></span>
				<span class="form-control-feedback"></span>
			  </div>
			  
			  <div class="row">
				<div class="col-xs-8">
				</div><!-- /.col -->
				<div class="col-xs-4">
				  <button type="button" id="btnSave" class="btn btn-primary btn-block btn-flat" onclick="reset_password()" >Reset</button>
				</div><!-- /.col -->
			  </div>
			</form>
      </section>
    </div>
  </section>
  <!-- footer -->
  <footer id="footer">
    <div class="text-center padder">
      <p>
        <small>Edumatic <br>&copy; <?=date('Y');?></small>
      </p>
    </div>
  </footer>
  
	<script>
		function reset_password()
		{
			$('#btnSave').text('changing...');
			$('#btnSave').attr('disabled',true);
			
			var url;

			url = "<?php echo site_url('login/reset_password')?>?mail=<?=$_GET['mail']?>";
			$.ajax({
				url : url,
				type: "POST",
				data: $('#form').serialize(),
				dataType: "JSON",
				success: function(data)
				{
					
					if(data.status) //if success close modal and reload ajax table
					{
						// $('#change_password').modal('hide');
						alert("Reset Password Successful !")
						window.location.href="<?=site_url('login')?>"	
					}
					else
					{
						for (var i = 0; i < data.inputerror.length; i++) 
						{
							$('[name="'+data.inputerror[i]+'"]').parent().parent().addClass('has-error'); //select parent twice to select div form-group class and add has-error class
							$('[name="'+data.inputerror[i]+'"]').next().text(data.error_string[i]); //select span help-block class set text error string
						}
					}
					$('#btnSave').text('change'); //change button text
					$('#btnSave').attr('disabled',false); //set button enable 


				},
				error: function (jqXHR, textStatus, errorThrown)
				{
					alert('Error adding / update data');
					$('#btnSave').text('change'); //change button text
					$('#btnSave').attr('disabled',false); //set button enable 

				}
			});
		}
			
	</script>
</body>