<?php $this->load->view('header')?>
<body class="theme-green">
    <div class="page-loader-wrapper">
        <div class="loader">
            <div class="md-preloader pl-size-md">
                <svg viewbox="0 0 75 75">
                    <circle cx="37.5" cy="37.5" r="33.5" class="pl-red" stroke-width="4" />
                </svg>
            </div>
            <p>Please wait...</p>
        </div>
    </div>
    <!-- #END# Page Loader -->
    <!-- Overlay For Sidebars -->
    <div class="overlay"></div>
    <!-- #END# Overlay For Sidebars -->
    <!-- Search Bar -->
    <div class="search-bar">
        <div class="search-icon">
            <i class="material-icons">search</i>
        </div>
        <input type="text" placeholder="START TYPING...">
        <div class="close-search">
            <i class="material-icons">close</i>
        </div>
    </div>
    <!-- #END# Search Bar -->
	<?php $this->load->view('header_menu')?>

		<?php $this->load->view('menu')?>
            <?=$title?>

		<section class="content">
        <div class="container-fluid">
		<div class="row clearfix">
                <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                    <div class="card">
                        <div class="header">
                            <h2>
                                <?=$title;?>
                            </h2>

                        </div>
                        <div class="body">
                            <?php $this->load->view($page);?>                            
                        </div>
                    </div>
                </div>
            </div>

		</div>
    </section>


	<?php //$this->load->view('footer')

  ?>
  </body>
</html>
