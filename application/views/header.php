<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<title>Koperasi</title>
	<meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
	<script src="<?php echo base_url('node_modules/socket.io/node_modules/socket.io-client/socket.io.js');?>"></script>
	<link rel="icon" href="<?=base_url('assets/images/logo.png')?>" type="image/png" sizes="16x16">
	<link href="<?=base_url('assets')?>/plugins/bootstrap/css/bootstrap.css" rel="stylesheet">

	<!-- Waves Effect Css -->
	<link href="<?=base_url('assets')?>/plugins/node-waves/waves.css" rel="stylesheet" />

	<!-- Animation Css -->
	<link href="<?=base_url('assets')?>/plugins/animate-css/animate.css" rel="stylesheet" />

	<!-- Preloader Css -->
	<link href="<?=base_url('assets')?>/plugins/material-design-preloader/md-preloader.css" rel="stylesheet" />

	<!-- Bootstrap Material Datetime Picker Css -->
	<link href="<?=base_url('assets')?>/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

	<!-- Wait Me Css -->
	<link href="<?=base_url('assets')?>/plugins/waitme/waitMe.css" rel="stylesheet" />

	<!-- Bootstrap Select Css -->
	<link href="<?=base_url('assets')?>/plugins/bootstrap-select/css/bootstrap-select.css" rel="stylesheet" />

	<!-- Custom Css -->
	<link href="<?=base_url('assets')?>/css/style.css" rel="stylesheet">

	<!-- AdminBSB Themes. You can choose a theme from css/themes instead of get all themes -->
	<link href="<?=base_url('assets')?>/css/themes/all-themes.css" rel="stylesheet" />
	<?php

	echo put_css_fonts();
		//echo put_css_plugins();
	echo put_css();
	echo put_js_plugins();
	echo put_js();

	?>

	<script>		// var socket = io.connect( 'http://'+window.location.hostname+':'+8008 );
		 // io.end();
		</script>
	</head>
