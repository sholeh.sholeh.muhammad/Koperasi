<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Product Category
    <small><?php echo (!$edit)?'Insert':'Edit' ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">Product Category</a></li>
    <li class="active"><?php echo (!$edit)?'Insert':'Edit' ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><strong>Error! </strong>'.$message.'</div></div>': '';
    ?>
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Product Category</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <form role="form" method="POST" enctype="multipart/form-data" class="form-horizontal">
            
			<div class="form-group <?php echo (form_error('parent_cat_id') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Parent Menu</label>
					<div class="col-md-10">
					<select class="form-control select2me" name="parent_cat_id" data-placeholder="Pilih..." >
						<option>--Select--</option>
						<?php
						foreach($product_category_list as $me){
							if($me->cat_id == set_value('parent_cat_id',$product_category->parent_cat_id)){
								echo "<option value='$me->cat_id' selected>$me->cat_name</option>";
							} else {
								echo "<option value='$me->cat_id'>$me->cat_name</option>";
							}
						}
						?>
					</select>
					</div>
					<!--<input class="form-control mask_number" name="parent_menu_id" value="<?php echo set_value('parent_menu_id', $menu->parent_menu_id); ?>" placeholder="Men Menu Id"  maxlength=10>-->
				<?php echo form_error('parent_menu_id'); ?>
			</div>
			<div class="form-group <?php echo (form_error('cat_name') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Category Name</label><div class="col-md-10"><input class="form-control " name="cat_name" value="<?php echo set_value('cat_name', $product_category->cat_name); ?>" placeholder="Category Name"  required  maxlength=50></div>
				<?php echo form_error('cat_name'); ?>
			</div>
			<div class="form-group <?php echo (form_error('cat_permalink') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Category Link</label><div class="col-md-10"><input class="form-control " name="cat_permalink" value="<?php echo set_value('cat_permalink', $product_category->cat_permalink); ?>" placeholder="Category Link"  required  maxlength=100></div>
				<?php echo form_error('cat_permalink'); ?>
			</div>
			<div class="form-group <?php echo (form_error('cat_status') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Status</label>
					<div class="col-md-10">
					<div class="radio">
			            <label>
			              <input type="radio" name="cat_status" value="1" <?php echo set_value('cat_status', ($product_category->cat_status == '1') ? "checked" : ""); ?>> Aktif
			            </label>
			            <label>
			              <input type="radio" name="cat_status" value="0" <?php echo set_value('cat_status', ($product_category->cat_status === '0') ? "checked" : ""); ?>> Tidak Aktif
			            </label>
			          </div>
			          </div>
				<?php echo form_error('cat_status'); ?>
			</div>
            <div class="box-footer">
               <a href="<?php echo $current_context; ?>" class="btn btn-default">Batal</a>
               <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            </div><!-- /.box-footer -->
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->