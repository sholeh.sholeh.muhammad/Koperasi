 <!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Products
    <small>list page</small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>admin/dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li class="active">Product</li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-check"></i> Great!</h4>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-4 col-md-offset-8" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><h4><i class="fa fa-exclamation-triangle"></i> Uh-Oh!</h4>'.$message.'</div></div>': '';
    ?>
    <div class="col-xs-12">
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Data Product <span class="badge"><?php echo $total_rows; ?> Data</span></h3>
          <div class="box-tools pull-right">
            <a href="<?php echo $current_context . 'add/'; ?>" class="btn btn-xs bg-light-blue">
                <i class="fa fa-plus"></i>&nbsp; Add
            </a>   
            <a href="javascript:;" class="btn btn-xs bg-orange"><i class="fa fa-refresh"></i> Refresh</a>
            <button class="btn btn-box-tool btn-xs" data-widget="collapse"><i class="fa fa-minus"></i></button>
          </div>
        </div><!-- /.box-header -->
        <div class="box-body">
          <div class="row">
            <div class="col-md-2">
              <dl class="text-right">
                <dt><h5>Filter Data</h5></dt>
                <dd>type/ select to find or filter data</dd>
              </dl>
            </div>
          <div class="col-md-10">
            <form method="post" action="<?php echo $current_context . 'search'; ?>">
                <?php $key = (object) $this->session->userdata('filter_product'); 
                //print_r($key);die();?>
                <div class="row">
                 <div class="col-md-4">
                    <div class="form-group">
                      <label class="control-label">Product Name</label>
                      <input class="form-control " name="product_name" value="<?php echo $key->product_name; ?>" placeholder="Product Name">
                    </div>
                  </div>
                    
                    
                    <div class="col-md-4">
                      <div class="form-group">
                        <label class="control-label">Product Status</label>
                        <select name="product_status" class="form-control select2" data-placeholder="Select Status">
                          <option value=""></option>
                          <option value="1" <?php echo ($key->product_status === '1') ? 'selected' : ''; ?>>Available</option>
                          <option value="0" <?php echo ($key->product_status === '0') ? 'selected' : ''; ?>>Not Available</option>
                        </select>
                      </div>
                    </div>
                  <div class="col-md-12">
                    <div class="clearfix pull-left">
                        <input type="hidden" id="search" name="search" value="true">
                        <button type="submit" class="btn btn-sm btn-primary">Search</button>
                        <button type="button" class="btn btn-sm btn-default" onclick="location.href='<?php echo $current_context; ?>'">Clear</button>
                    </div>
                  </div>
                </div> 
              </form>
          </div>
         </div>
        </div><!-- /.box-body -->
        <div class="box-body">
          <div class="row">
           <div class="col-md-2">
              <dl class="text-right">
                
                  <dt><h5>Data</h5></dt>
                  <dd>data table, all data restore here</dd>
              
              </dl>
              <div class="clearfix text-right">
                <p><a href="<?php echo $current_context . 'add/'; ?>" class="btn btn-sm bg-light-blue btn-block">
                    <i class="fa fa-plus"></i>&nbsp; Add Product
                </a></p>
                <p>
                  
                  <span class="slctd-dta hide"></span>
                </p>
              </div>
            </div>

        <div class="col-md-10">  
         <div class="table-responsive">
          <table class="table table-bordered table-hover table-striped" id="table_data">
          <thead>
            <tr> 
              <th class="table-checkbox">
                No.
              </th>
              <th>Pictures</th>
              <th>Information</th>
              <!-- <th>Price</th>
              <th>Stock</th> -->
              <th>Price</th>
              <th>Status</th>
              <th>Action</th>
            </tr>
          </thead>
            <?php
            if(!empty($product)){
              $i = 1;
            $no = $offset+1;
            foreach ($product as $row) {  ?>
                
                <tr>
                    <td>
                    <?php echo $i ?>
                    </td>
      							<td>
                      <?php 
                          if (!empty($row->product_photo) && (file_exists($row->product_photo))) {
                              $photo = base_url(). $row->product_photo;
                          } else {
                              $photo = base_url() . "default/default.jpg";
                          }
                      ?>
                      <p><img src="<?php echo $photo ?>" width="80"></p>
                    </td>
      							<td class="no-padi-p">
                        <p><strong><?php echo $row->product_name; ?></strong></p>
                        <!-- <p><?php //echo $row->product_description; ?></p> -->
                        <p><i class="fa fa-fw fa-tags"></i> <?php echo (!empty($row->cat_permalink)?$row->cat_permalink:'<i>(empty)</i>'); ?></p>
                        <?php if(!empty($row->product_discount)){?>
                        <p><span class="label label-success"><?php echo "Discount ".$row->product_discount."%"; ?></span></p>
                        <?php }?>
                        <p>
                          
                        </p>
                        <p>
                          <span><b>Global Stock: </b><?php echo ($row->product_stock)?:0; ?></span>
                        </p>
                    </td>
                    <td>Rp <?php echo number_format($row->product_price); ?></td>

                   <!--  <td>Rp <?php echo number_format($row->product_price); ?></td>
                    <td><?php echo $row->product_stock; ?></td> -->
      							<td><?php 
                      $status = $row->product_status;
                        if($status == 1){
                          ?>
                            <span class="label label-success">Available</span>
                        <?php }
                        if($status == 0){
                          ?>
                            <span class="label label-danger">Not Available</span>
                        <?php }
                        ?> 
              </td>

							<td class="td-btn">
              <?php 
                $status = $this->uri->segment(3);
                $page = $this->uri->segment(4);
              ?>
								<!-- a href="<?php echo $current_context . 'detail'  .'/'. $row->product_id ?>" class="badge bg-yellow"><i class="fa fa-eye fa-fw"></i> lihat</a-->
								<p><a href="<?php echo $current_context . 'edit'  .'/'. $row->product_id . '/' . @$status . '/' . @$page ?>" class="badge bg-green"><i class="fa fa-edit fa-fw"></i> view</a></p>
                <!-- <?php if($is_ordered[$i] == 'kosong'){ ?>
								<p><a href="#" data-href="<?php echo $current_context . 'delete'  .'/'. $row->product_id ?>" data-toggle="modal" data-target="#deleteModal"  class="badge bg-red"><i class="fa fa-trash-o fa-fw"></i> delete</a></p>
                <?php } ?> -->
							</td>
                </tr>
            <?php 
            $no++; $i++;
            } 
          } else if($this->uri->segment(3) == 'search'){
          ?>

           <tr>
                        <td colspan="6" class="empty-table">
                          <br>
                          <p class="text-center"><i class="fa fa-th-list fa-3x"></i></p>
                          <p class="text-center"><i>We couldn't find the data you're looking for.</p></i>
                          <br>
                        </td>
                      </tr>
          <?php
          } else {
            ?>

          <tr>
             <td colspan="6" class="empty-table">
               <br>
                <p class="text-center"><i class="fa fa-th-list fa-3x"></i></p>
                <p class="text-center"><i><a href="<?php echo $current_context . 'add/'; ?>">Add</a> your product now.</p></i> 
                 <br>
              </td>
           </tr>
            <?php
            //die();
          }
          ?>
          <tfoot>
                      <tr>
                        <th class="table-checkbox">
                          
                        </th>
                        <th>Pictures</th>
                        <th>Information</th>
                        <!-- <th>Price</th>
                        <th>Stock</th> -->
                        <th>Price</th>
                        <th>Status</th>
                        <th>Action</th>
                      </tr>
                    </tfoot>
          </table>
          </div> 
      
        </div> 
       </div>  
       </div><!-- /.box-body -->
        <div class="box-footer clearfix">
            <div class="col-sm-6">
              
            </div>
            <div class="col-sm-6">
              <div class="pull-right">
              <?php echo $pagination; ?>
              </div>
            </div>
        </div>
      </div><!-- /.box -->
    </div>
  </div>
    <div class="row">
      <div class="col-md-6 col-md-offset-3 col-md-offset-3">
        <p></p>
        <p class="text-center icon-page"><i class="fa fa-th-list fa-5x"></i></p>
        <br>
        <div class="callout callout-info">
          <h4><i class="fa fa-bullhorn fa-fw"></i> Learn More</h4>
          <p>Learn More about product <a href="">here</a>. Or <a href="">contact us</a> for more information</p>
        </div>
      </div>
    </div>
</section><!-- /.content -->