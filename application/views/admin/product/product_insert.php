<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    Product Category
    <small><?php echo (!$edit)?'Insert':'Edit' ?></small>
  </h1>
  <ol class="breadcrumb">
    <li><a href="<?php echo base_url(); ?>dashboard"><i class="fa fa-dashboard"></i> Dashboard</a></li>
    <li><a href="<?php echo $current_context; ?>">Product Category</a></li>
    <li class="active"><?php echo (!$edit)?'Insert':'Edit' ?></li>
  </ol>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <?php
        $message = $this->session->flashdata('message');
        $type_message = $this->session->flashdata('type_message');
        echo (!empty($message) && $type_message=="success") ? ' <div class="col-md-12" id="data-alert-box"><div class="alert alert-success"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><strong>Berhasil! </strong>'.$message.'</div></div>': '';
        echo (!empty($message) && $type_message=="error") ? '   <div class="col-md-12" id="data-alert-box"><div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times</button><strong>Error! </strong>'.$message.'</div></div>': '';
    ?>
    <!-- right column -->
    <div class="col-md-12">
      <!-- general form elements disabled -->
      <div class="box box-warning">
        <div class="box-header with-border">
          <h3 class="box-title">Product Category</h3>
        </div><!-- /.box-header -->
        <div class="box-body">
          <form role="form" method="POST" enctype="multipart/form-data" class="form-horizontal">
            
			<div class="form-group <?php echo (form_error('cat_id') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Category Product</label>
					<div class="col-md-10">
					<select class="form-control select2me" name="cat_id" data-placeholder="Pilih..." >
						<option>--Select--</option>
						<?php
						foreach($product_category_list as $me){
							if($me->cat_id == set_value('cat_id',$product_category->cat_id)){
								echo "<option value='$me->cat_id' selected>$me->cat_name</option>";
							} else {
								echo "<option value='$me->cat_id'>$me->cat_name</option>";
							}
						}
						?>
					</select>
					</div>
					<!--<input class="form-control mask_number" name="parent_menu_id" value="<?php echo set_value('parent_menu_id', $menu->parent_menu_id); ?>" placeholder="Men Menu Id"  maxlength=10>-->
				<?php echo form_error('parent_menu_id'); ?>
			</div>
			<div class="form-group <?php echo (form_error('product_name') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Product Name</label><div class="col-md-10"><input class="form-control " name="product_name" value="<?php echo set_value('product_name', $product->product_name); ?>" placeholder="Product Name"  required  maxlength=50></div>
				<?php echo form_error('product_name'); ?>
			</div>
			<div class="form-group <?php echo (form_error('product_description') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Product Description</label><div class="col-md-10"><textarea data-rule-required="true" data-rule-minlength="5" class="form-control" rows="8" name="product_description" ><?php echo set_value('product_description', $product->product_description); ?></textarea></div>
				<?php echo form_error('product_description'); ?>
			</div>
			<div class="form-group <?php echo (form_error('product_photo') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Product Photo</label><div class="col-md-10">
						<input type="file" name="product_photo" class="form-control">
					</div>
				<?php echo form_error('product_photo'); ?>
			</div>
            
			<div class="form-group <?php echo (form_error('product_price') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Product Price</label><div class="col-md-10"><input class="form-control " name="product_price" value="<?php echo set_value('product_price', $product->product_price); ?>" placeholder="Product Price" type="number" required  maxlength=50></div>
				<?php echo form_error('product_price'); ?>
			</div>
			<div class="form-group <?php echo (form_error('product_stock') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Product Stock</label><div class="col-md-10"><input class="form-control " name="product_stock" value="<?php echo set_value('product_stock', $product->product_stock); ?>" placeholder="Product Stock" type="number" required  maxlength=50></div>
				<?php echo form_error('product_stock'); ?>
			</div>
			<div class="form-group <?php echo (form_error('product_discount') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Product Discount</label><div class="col-md-10"><input class="form-control " name="product_discount" value="<?php echo set_value('product_discount', $product->product_discount); ?>" placeholder="Dalam %" type="number" required  maxlength=50></div>
				<?php echo form_error('product_discount'); ?>
			</div>
			<div class="form-group <?php echo (form_error('product_weight') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Product Weight</label><div class="col-md-10"><input class="form-control " name="product_weight" value="<?php echo set_value('product_weight', $product->product_weight); ?>" placeholder="Product Weight" type="number" required  maxlength=50></div>
				<?php echo form_error('product_weight'); ?>
			</div>
			<div class="form-group <?php echo (form_error('product_status') != "") ? "has-error" : "" ?>">
					<label class="control-label col-md-2">Status</label>
					<div class="col-md-10">
					<div class="radio">
			            <label>
			              <input type="radio" name="product_status" value="1" <?php echo set_value('product_status', ($product->product_status == '1') ? "checked" : ""); ?>> Aktif
			            </label>
			            <label>
			              <input type="radio" name="product_status" value="0" <?php echo set_value('product_status', ($product->product_status === '0') ? "checked" : ""); ?>> Tidak Aktif
			            </label>
			          </div>
			          </div>
				<?php echo form_error('product_status'); ?>
			</div>
            <div class="box-footer">
               <a href="<?php echo $current_context; ?>" class="btn btn-default">Batal</a>
               <button type="submit" class="btn btn-primary pull-right">Simpan</button>
            </div><!-- /.box-footer -->
          </form>
        </div><!-- /.box-body -->
      </div><!-- /.box -->
    </div><!--/.col (right) -->
  </div>   <!-- /.row -->
</section><!-- /.content -->